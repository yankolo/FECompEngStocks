package com.dawson.ces.notes.persistence;

import android.app.Application;
import android.arch.lifecycle.LiveData;
import android.os.AsyncTask;

import com.dawson.ces.notes.data.Note;

import java.util.List;

/**
 * Manages note data
 *
 * @author Huan
 */
public class NoteRepository {

    private NoteDAO noteDAO;
    private LiveData<List<Note>> notes;

    public NoteRepository(Application application) {
        NoteRoomDatabase database = NoteRoomDatabase.getDatabase(application);
        noteDAO = database.noteDAO();
        notes = noteDAO.getAllNotes();
    }

    public LiveData<List<Note>> getAllNotes() {
        return notes;
    }

    public void insert(Note note) {
        new insertAsyncTask(noteDAO).execute(note);
    }

    public void update(Note note) {
        new updateAsyncTask(noteDAO).execute(note);
    }

    public void delete(Note note) {
        new deleteAsyncTask(noteDAO).execute(note);
    }

    private static class insertAsyncTask extends AsyncTask<Note, Void, Void> {
        private NoteDAO noteDAO;

        insertAsyncTask(NoteDAO dao) {
            this.noteDAO = dao;
        }

        @Override
        protected Void doInBackground(final Note... notes) {
            this.noteDAO.insert(notes[0]);
            return null;
        }
    }

    private static class updateAsyncTask extends AsyncTask<Note, Void, Void> {
        private NoteDAO noteDAO;

        updateAsyncTask(NoteDAO dao) {
            this.noteDAO = dao;
        }

        @Override
        protected Void doInBackground(final Note... notes) {
            this.noteDAO.update(notes[0]);
            return null;
        }
    }

    private static class deleteAsyncTask extends AsyncTask<Note, Void, Void> {
        private NoteDAO noteDAO;

        deleteAsyncTask(NoteDAO dao) {
            this.noteDAO = dao;
        }

        @Override
        protected Void doInBackground(final Note... notes) {
            this.noteDAO.delete(notes[0]);
            return null;
        }
    }
}
