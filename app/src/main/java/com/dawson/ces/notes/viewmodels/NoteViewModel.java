package com.dawson.ces.notes.viewmodels;

import android.app.Application;
import android.arch.lifecycle.AndroidViewModel;
import android.arch.lifecycle.LiveData;

import com.dawson.ces.notes.data.Note;
import com.dawson.ces.notes.persistence.NoteRepository;

import java.util.List;

/**
 * Provides data to the UI. Acts as a communication center between the Repository and the UI.
 * Hides where the data originates from the UI.
 *
 * @author Huan
 */
public class NoteViewModel extends AndroidViewModel {

    private NoteRepository noteRepository;

    private LiveData<List<Note>> notes;

    public NoteViewModel(Application application) {
        super(application);
        noteRepository = new NoteRepository(application);
        notes = noteRepository.getAllNotes();
    }

    public LiveData<List<Note>> getAllNotes() {
        return notes;
    }

    public void insert(Note note) {
        noteRepository.insert(note);
    }

    public void update(Note note) {
        noteRepository.update(note);
    }

    public void delete(Note note) {
        noteRepository.delete(note);
    }
}
