package com.dawson.ces.notes.activities;

import android.content.Intent;
import android.os.Bundle;
import android.support.design.widget.FloatingActionButton;
import android.support.v7.app.AppCompatActivity;
import android.text.TextUtils;
import android.view.View;
import android.widget.EditText;

import com.dawson.ces.feces.R;


/**
 * Activity to add or edit notes
 *
 * @author Huan
 */
public class AddEditNoteActivity extends AppCompatActivity {

    public static final String EXTRA_ID = "com.dawson.ces.note.activities.AddEditNoteActivity.ID";
    public static final String EXTRA_REPLY = "com.dawson.ces.note.activities.AddEditNoteActivity.REPLY";

    private EditText editText;

    @Override
    public void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_add_edit_note);
        editText = findViewById(R.id.edit_note);

        Intent intent = getIntent();

        if(intent.hasExtra(EXTRA_ID)) {
            editText.setText(intent.getStringExtra(EXTRA_REPLY));
        }

        FloatingActionButton floatingActionButton = findViewById(R.id.fab_save);
        floatingActionButton.setOnClickListener(new View.OnClickListener() {
            public void onClick(View view) {
                Intent replyIntent = new Intent();
                if (TextUtils.isEmpty(editText.getText())) {
                    setResult(RESULT_CANCELED, replyIntent);
                } else {
                    String note = editText.getText().toString();
                    replyIntent.putExtra(EXTRA_REPLY, note);

                    int id = getIntent().getIntExtra(EXTRA_ID, -1);
                    if (id != -1) {
                        replyIntent.putExtra(EXTRA_ID, id);
                    }

                    setResult(RESULT_OK, replyIntent);
                }
                finish();
            }
        });
    }
}