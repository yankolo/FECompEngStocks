package com.dawson.ces.notes.persistence;

import android.arch.lifecycle.LiveData;
import android.arch.persistence.room.Dao;
import android.arch.persistence.room.Delete;
import android.arch.persistence.room.Insert;
import android.arch.persistence.room.Query;
import android.arch.persistence.room.Update;

import com.dawson.ces.notes.data.Note;

import java.util.List;

/**
 * Database access object that maps SQL queries to methods
 *
 * @author Huan
 */
@Dao
public interface NoteDAO {

    @Insert
    void insert(Note note);

    @Update
    void update(Note note);

    @Delete
    void delete(Note note);

    @Query("DELETE FROM note")
    void deleteAll();

    @Query("SELECT * from note")
    LiveData<List<Note>> getAllNotes();

}
