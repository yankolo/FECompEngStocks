package com.dawson.ces.stocks.fragments;

import android.content.Context;
import android.os.AsyncTask;
import android.os.Bundle;
import android.support.annotation.NonNull;
import android.support.annotation.Nullable;
import android.support.design.widget.Snackbar;
import android.support.v4.app.Fragment;
import android.support.v4.app.FragmentTransaction;
import android.view.KeyEvent;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.Button;
import android.widget.EditText;
import android.widget.TextView;

import com.dawson.ces.feces.R;
import com.dawson.ces.stocks.data.Stock;
import com.dawson.ces.stocks.exceptions.SellException;
import com.dawson.ces.stocks.repositories.PortfolioRepository;

import org.w3c.dom.Text;

/**
 * The SellStockFragment represents the fragment that is displayed when the "Sell" button
 * is pressed in the StockDetailsFragment. The SellStockFragment displays an input that
 * asks the user to input a quantity of stocks to sell. It performs validation on the input
 * prior to activating the "Confirm Sell" button.
 *
 * @author Yanik
 */
public class SellStockFragment extends Fragment {

    //================================================================================
    // Properties
    //================================================================================

    private Stock stock;
    private double balance;
    private int quantityOwned;
    private int quantitySold;
    private TextView textViewStocksLeft;
    private TextView textViewGain;
    private Button sellButton;
    private EditText editTextQuantity;

    //================================================================================
    // Fragment Instantiation
    //================================================================================

    public static SellStockFragment newInstance(Context context, Stock stock, double balance, int quantityOwned) {
        SellStockFragment sellStockFragment = new SellStockFragment();

        Bundle args = new Bundle();
        args.putSerializable("stock", stock);
        args.putDouble("balance", balance);
        args.putInt("quantity_owned", quantityOwned);
        sellStockFragment.setArguments(args);

        return sellStockFragment;
    }

    //================================================================================
    // Lifecycle Methods
    //================================================================================

    @Override
    public void onCreate(@Nullable Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        stock = (Stock) this.getArguments().getSerializable("stock");
        balance = this.getArguments().getDouble("balance");
        quantityOwned = this.getArguments().getInt("quantity_owned");
    }

    @Override
    public View onCreateView(LayoutInflater inflater, ViewGroup container,
                             Bundle savedInstanceState) {
        View view = inflater.inflate(R.layout.stocks_sell, container, false);
        return view;
    }

    @Override
    public void onViewCreated(@NonNull View view, @Nullable Bundle savedInstanceState) {
        super.onViewCreated(view, savedInstanceState);

        // Finding all the necessary views
        textViewStocksLeft = view.findViewById(R.id.stocks_left);
        textViewStocksLeft.setText(String.valueOf(this.quantityOwned));
        textViewGain = view.findViewById(R.id.gain);
        textViewGain.setText("None");
        editTextQuantity = view.findViewById(R.id.input_name);
        sellButton = view.findViewById(R.id.buy_confirm_button);

        // Assigning listeners
        sellButton.setOnClickListener(createSellButtonClickListener());
        editTextQuantity.setOnKeyListener(createInputKeyListener(view));
    }

    //================================================================================
    // Listeners
    //================================================================================

    private View.OnClickListener createSellButtonClickListener() {
        return (v) -> {
            quantitySold = Integer.parseInt(editTextQuantity.getText().toString());
            SellStockFragment.SellTask sellTask = new SellStockFragment.SellTask();
            sellTask.execute();
        };
    }

    private View.OnKeyListener createInputKeyListener(View view) {
        return (v, keyCode, event) -> {
            char s = event.getDisplayLabel();
            if (event.getAction() != KeyEvent.ACTION_DOWN && keyCode == KeyEvent.KEYCODE_DEL) {
                if (!((EditText) v).getText().toString().isEmpty())
                    calculate(' ');
            }
            if (event.getAction() != KeyEvent.ACTION_DOWN)
                return true;
            if (keyCode == KeyEvent.KEYCODE_DEL) {
                if (((EditText) v).getText().toString().length() <= 1) {
                    textViewStocksLeft.setText(String.valueOf(quantityOwned));
                    textViewGain.setText("None");
                    sellButton.setEnabled(false);
                }
            }
            if (Character.isDigit(s)) {
                calculate(s);
            }
            return false;
        };
    }

    //================================================================================
    // Helper Methods
    //================================================================================

    private void calculate(char valueChar){
        Button buy = getView().findViewById(R.id.buy_confirm_button);
        String input = ((EditText) getView().findViewById(R.id.input_name)).getText().toString();
        input += valueChar;
        input= input.trim();
        int value = Integer.parseInt(input);
        int newBalanceQuantity = this.quantityOwned - value;
        TextView textViewStocksLeft = getView().findViewById(R.id.stocks_left);
        TextView textViewGain = getView().findViewById(R.id.gain);
        if (newBalanceQuantity < 0) {
            textViewStocksLeft.setText("Not Enough");
            textViewGain.setText("None");
            buy.setEnabled(false);
        } else {
            textViewStocksLeft.setText(String.valueOf(newBalanceQuantity));
            this.quantitySold = value;
            double gain = value * this.stock.getPrice();
            double finalGain = (double) Math.round(gain * 100.0) / 100.0;
            this.balance +=finalGain;
            textViewGain.setText(String.valueOf(finalGain));
            buy.setEnabled(true);
        }
    }

    //================================================================================
    // Async Tasks
    //================================================================================

    private class SellTask extends AsyncTask<Void, Void, Void> {
        private boolean errorWhileBuying;
        private String error;
        private double cashleft;


        @Override
        protected Void doInBackground(Void... voids) {
            PortfolioRepository portfolioRepository = new PortfolioRepository(getContext());

            try {
                cashleft = portfolioRepository.sellStock(stock.getSymbol(), quantitySold);
            } catch (SellException e) {
                errorWhileBuying = true;
                error = e.getMessage();
            }

            return null;
        }

        @Override
        protected void onPostExecute(Void aVoid) {
            FragmentTransaction transaction = getFragmentManager().beginTransaction();
            transaction.remove(SellStockFragment.this);
            transaction.commit();

            if (errorWhileBuying) {
                Snackbar.make(getActivity().findViewById(android.R.id.content),
                        error, Snackbar.LENGTH_SHORT).show();
            } else {
                ((StockDetailsFragment)SellStockFragment.this.getParentFragment()).updateBalance(cashleft);
                Snackbar.make(getActivity().findViewById(android.R.id.content),
                        R.string.buy_success_snackbar, Snackbar.LENGTH_SHORT).show();
            }
        }
    }

}
