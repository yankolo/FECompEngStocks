package com.dawson.ces.stocks.exceptions;

public class SellException extends Exception {
    public SellException(String message) {
        super(message);
    }
}
