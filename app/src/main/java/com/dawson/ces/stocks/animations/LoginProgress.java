package com.dawson.ces.stocks.animations;

import android.animation.Animator;
import android.animation.AnimatorListenerAdapter;
import android.content.Context;
import android.view.View;

/**
 * This class in charge of displaying/hiding the progress animation when the user logs in
 *
 * @author Yanik
 */
public class LoginProgress {

    //================================================================================
    // Properties
    //================================================================================

    private Context context;
    private View progressView;
    private View loginFormView;

    //================================================================================
    // Constructors
    //================================================================================

    public LoginProgress(Context context, View progressView, View loginFormView) {
        this.context = context;
        this.progressView = progressView;
        this.loginFormView = loginFormView;
    }

    //================================================================================
    // Public Methods
    //================================================================================

    public void showProgress() {
        int shortAnimTime = context.getResources().getInteger(android.R.integer.config_shortAnimTime);

        loginFormView.setVisibility(View.GONE);
        loginFormView.animate().setDuration(shortAnimTime).alpha(0).setListener(
                new AnimatorListenerAdapter() {
                    @Override
                    public void onAnimationEnd(Animator animation) {
                        loginFormView.setVisibility(View.GONE);
                    }
        });

        progressView.setVisibility(View.VISIBLE);
        progressView.animate().setDuration(shortAnimTime).alpha(1).setListener(
                new AnimatorListenerAdapter() {
                    @Override
                    public void onAnimationEnd(Animator animation) {
                        progressView.setVisibility(View.VISIBLE);
                    }
        });
    }

    public void hideProgress() {
        int shortAnimTime = context.getResources().getInteger(android.R.integer.config_shortAnimTime);

        loginFormView.setVisibility(View.VISIBLE);
        loginFormView.animate().setDuration(shortAnimTime).alpha(1).setListener(
                new AnimatorListenerAdapter() {
                    @Override
                    public void onAnimationEnd(Animator animation) {
                        loginFormView.setVisibility(View.VISIBLE);
                    }
        });

        progressView.setVisibility(View.GONE);
        progressView.animate().setDuration(shortAnimTime).alpha(0).setListener(
                new AnimatorListenerAdapter() {
                    @Override
                    public void onAnimationEnd(Animator animation) {
                        progressView.setVisibility(View.GONE);
                    }
        });
    }
}
