package com.dawson.ces.stocks.adapters;

import android.content.Context;
import android.support.annotation.NonNull;
import android.support.v4.app.FragmentManager;
import android.support.v4.app.FragmentTransaction;
import android.support.v7.widget.RecyclerView;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.TextView;

import com.dawson.ces.feces.R;
import com.dawson.ces.stocks.data.Stock;
import com.dawson.ces.stocks.fragments.StockDetailsFragment;

import java.util.List;

/**
 * Adapter that is used to link a RecyclerView with Stocks
 *
 * @author Yanik
 */
public class StockListAdapter extends RecyclerView.Adapter<RecyclerView.ViewHolder> {

    //================================================================================
    // View Holders
    //================================================================================

    public class StockViewHolder extends RecyclerView.ViewHolder {
        TextView stockSymbol;
        TextView companyName;
        TextView stockPrice;
        TextView changePercentage;


        StockViewHolder(View view) {
            super(view);
            this.stockSymbol = (TextView) view.findViewById(R.id.stock_symbol);
            this.companyName = (TextView) view.findViewById(R.id.company_name);
            this.stockPrice = (TextView) view.findViewById(R.id.stock_price);
            this.changePercentage = (TextView) view.findViewById(R.id.stock_percentage_change);
        }
    }

    //================================================================================
    // Properties
    //================================================================================

    private Context context;
    private FragmentManager fragmentManager;
    private List<Stock> stocks;

    //================================================================================
    // Constructors
    //================================================================================

    public StockListAdapter(Context context, FragmentManager fragmentManager, List<Stock> stocks) {
        this.context = context;
        this.fragmentManager = fragmentManager;
        this.stocks = stocks;
    }

    //================================================================================
    // RecyclerView Overridden Methods
    //================================================================================

    @NonNull
    @Override
    public RecyclerView.ViewHolder onCreateViewHolder(@NonNull ViewGroup viewGroup, int viewType) {
        LayoutInflater layoutInflater = LayoutInflater.from(viewGroup.getContext());

        View stockListItem = layoutInflater.inflate(R.layout.stocks_list_item, viewGroup, false);
        return new StockViewHolder(stockListItem);
    }

    @Override
    public void onBindViewHolder(@NonNull RecyclerView.ViewHolder viewHolder, int position) {
        Stock stockToDisplay = stocks.get(position);
        StockViewHolder stockViewHolder = (StockViewHolder) viewHolder;

        stockViewHolder.stockSymbol.setText(stockToDisplay.getSymbol());
        stockViewHolder.companyName.setText(stockToDisplay.getCompanyName());
        stockViewHolder.stockPrice.setText(String.valueOf(stockToDisplay.getPrice()));
        stockViewHolder.changePercentage.setText(String.valueOf(stockToDisplay.getChangePercentage()));

        viewHolder.itemView.setOnClickListener(CreateItemClickListener(stocks.get(position)));
    }

    @Override
    public int getItemCount() {
        return stocks.size();
    }

    private View.OnClickListener CreateItemClickListener(Stock stock) {
        return (view) -> {
            StockDetailsFragment stockDetailsFragment = StockDetailsFragment.newInstance(context, stock);

            FragmentTransaction transaction = fragmentManager.beginTransaction();
            transaction.addToBackStack(null);
            transaction.replace(R.id.content_frame, stockDetailsFragment);

            transaction.commit();
        };
    }

    //================================================================================
    // Accessors
    //================================================================================

    public List<Stock> getStocks() {
        return stocks;
    }
}
