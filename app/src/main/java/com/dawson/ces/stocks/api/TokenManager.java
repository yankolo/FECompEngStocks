package com.dawson.ces.stocks.api;

import android.content.Context;
import android.content.SharedPreferences;

import com.dawson.ces.feces.R;

import org.json.JSONObject;


/**
 * The token manager manges the storing and updating of the token
 *
 * In order to update the token, the TokenManager assumes there is an email
 * and password in the SharedPreferences
 */
public class TokenManager {
    private Context context;

    public TokenManager(Context context) {
        this.context = context;
    }

    /**
     * Stores the token in SharedPreferences. In addition, it stores the time it takes
     * for the token to expire, and the timestamp that the token was generated on.
     * This is stored so that we can validate if the token is valid.
     *
     * @param token The JWT token to store
     * @param expiresIn The time it takes for the token to expire in seconds
     */
    public void storeToken(String token, int expiresIn) {
        // Store the token into shared preferences
        SharedPreferences.Editor prefsEditor = context.getSharedPreferences(
                context.getString(R.string.prefs_settings), Context.MODE_PRIVATE).edit();

        prefsEditor.putString(context.getString(R.string.prefs_user_token), token);
        prefsEditor.putInt(context.getString(R.string.prefs_expires_in), expiresIn);
        prefsEditor.putLong(context.getString(R.string.prefs_timestamp), System.currentTimeMillis() / 1000L);

        prefsEditor.apply();
    }

    /**
     * Validates the token by looking at the timestamp.
     * If the token is not valid, it makes an API call to get a new token
     */
    public void validateToken() {
        SharedPreferences prefs =
                context.getSharedPreferences(context.getString(R.string.prefs_settings), Context.MODE_PRIVATE);
        String token = prefs.getString(context.getString(R.string.prefs_user_token), "");
        int expiresIn = prefs.getInt(context.getString(R.string.prefs_expires_in), 0);
        long timestamp = prefs.getLong(context.getString(R.string.prefs_timestamp), 0L);

        long currentTimestamp = System.currentTimeMillis() / 1000L;

        // If the following is true, it means the token has expired
        if (timestamp + expiresIn < currentTimestamp){
            String email = prefs.getString(context.getString(R.string.prefs_email), "");
            String password = prefs.getString(context.getString(R.string.prefs_password), "");

            JSONObject json = PortfolioAPI.login(email, password);

            // We are sure that the email and password are correct, since they are only
            // stored when a login is successful. Therefore, not verifying they're validity
            token = json.optString("access_token");
            expiresIn = json.optInt("expires_in");

            storeToken(token, expiresIn);
        }
    }
}
