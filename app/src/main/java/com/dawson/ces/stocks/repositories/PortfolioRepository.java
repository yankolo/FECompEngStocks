package com.dawson.ces.stocks.repositories;

import android.content.Context;
import android.content.SharedPreferences;

import com.dawson.ces.feces.R;
import com.dawson.ces.stocks.api.PortfolioAPI;
import com.dawson.ces.stocks.api.TokenManager;
import com.dawson.ces.stocks.data.PortfolioItem;
import com.dawson.ces.stocks.data.Stock;
import com.dawson.ces.stocks.exceptions.BuyException;
import com.dawson.ces.stocks.exceptions.SellException;

import org.json.JSONArray;
import org.json.JSONException;
import org.json.JSONObject;

import java.util.ArrayList;
import java.util.List;

/**
 * The PortfolioRepository provides a data source abstraction for the portfolio. Use
 * this class to interact with the user's portfolio.
 *
 * In our case, the PortfolioRepository uses the PortfolioAPI behind the scene to
 * get its data.
 *
 * @author Yanik
 */
public class PortfolioRepository {

    //================================================================================
    // Properties
    //================================================================================

    private TokenManager tokenManager;
    private Context context;
    private String userToken;

    //================================================================================
    // Constructors
    //================================================================================

    public PortfolioRepository(Context context) {
        this.context = context;
        this.tokenManager = new TokenManager(context);
        initUserToken();
    }

    //================================================================================
    // Public Methods
    //================================================================================

    /**
     * Gets the user's portfolio items. The stocks in the portfolio items need to be updated with
     * new market information as they will only have their ticker symbols set.
     *
     * @return Returns the user's portfolio
     */
    public PortfolioItem[] getPortfolioItems() {
        tokenManager.validateToken();
        JSONArray response = PortfolioAPI.requestAllStocks(userToken);
        return toPortfolioItems(response);
    }

    /**
     * Gets the user's balance.
     *
     * @return Returns the users balance
     */
    public double getBalance() {
        tokenManager.validateToken();
        double balance = 0;
        try {
            JSONObject response = PortfolioAPI.requestBalance(userToken);
            balance = response.getDouble("cash");
        } catch (JSONException e) {
            e.printStackTrace();
        }

        return balance;
    }

    /**
     * Buys a stock
     *
     * @param ticker The ticker symbol of the stock to buy
     * @param quantity The quantity to buy
     * @return Returns the balance left
     */
    public double buyStock(String ticker, int quantity) throws BuyException{
        tokenManager.validateToken();
        double balance = 0;
        try {
            JSONObject response = PortfolioAPI.requestBuyTransaction(userToken, ticker, quantity);

            if (!response.has("cashleft")) {
                throw new BuyException(response.getString("error"));
            }

            balance = response.getDouble("cashleft");
        } catch (JSONException e) {
            e.printStackTrace();
        }

        return balance;
    }

    /**
     * Sells a stock
     *
     * @param ticker The ticker symbol of the stock to sell
     * @param quantity The quantity to sell
     * @return Returns the balance of the sell
     */
    public double sellStock(String ticker, int quantity) throws SellException {
        tokenManager.validateToken();
        double balance = 0;
        try {
            JSONObject response = PortfolioAPI.requestSellTransaction(userToken, ticker, quantity);

            if (!response.has("cashleft")) {
                throw new SellException(response.getString("error"));
            }

            balance = response.getDouble("cashleft");
        } catch (JSONException e) {
            e.printStackTrace();
        }

        return balance;
    }

    //================================================================================
    // Helper Methods
    //================================================================================

    private void initUserToken() {
        // Get the JWT token from shared preferences
        SharedPreferences prefs =
                context.getSharedPreferences(context.getString(R.string.prefs_settings), Context.MODE_PRIVATE);
        userToken = prefs.getString(context.getString(R.string.prefs_user_token), "");
    }

    private PortfolioItem[] toPortfolioItems(JSONArray json) {
        List<PortfolioItem> portfolioItems = new ArrayList<>();

        try {
            for (int i = 0; i < json.length(); i++) {
                JSONObject stockData = json.getJSONObject(i);

                Stock stock = new Stock();
                stock.setSymbol(stockData.getString("ticker"));

                int quantity = stockData.getInt("quantity");

                PortfolioItem portfolioItem = new PortfolioItem(stock, quantity);

                portfolioItems.add(portfolioItem);
            }
        } catch (JSONException e) {
            e.printStackTrace();
        }

        return portfolioItems.toArray(new PortfolioItem[0]);
    }
}
