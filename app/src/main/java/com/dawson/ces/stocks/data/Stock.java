package com.dawson.ces.stocks.data;

import java.io.Serializable;

/**
 * A Stock represents all the information concerning the stock of a given company.
 *
 * @author Yanik
 */
public class Stock implements Serializable {

    //================================================================================
    // Properties
    //================================================================================

    private String symbol;
    private String companyName;
    private double price;
    private double changePercentage;
    private String currency;
    private String stockExchange;

    //================================================================================
    // Accessors
    //================================================================================

    public String getSymbol() {
        return symbol;
    }

    public void setSymbol(String symbol) {
        this.symbol = symbol;
    }

    public String getCompanyName() {
        return companyName;
    }

    public void setCompanyName(String companyName) {
        this.companyName = companyName;
    }

    public double getPrice() {
        return price;
    }

    public void setPrice(double price) {
        this.price = price;
    }

    public double getChangePercentage() {
        return changePercentage;
    }

    public void setChangePercentage(double changePercentage) {
        this.changePercentage = changePercentage;
    }

    public String getCurrency() {
        return currency;
    }

    public void setCurrency(String currency) {
        this.currency = currency;
    }

    public String getStockExchange() {
        return stockExchange;
    }

    public void setStockExchange(String stockExchange) {
        this.stockExchange = stockExchange;
    }
}
