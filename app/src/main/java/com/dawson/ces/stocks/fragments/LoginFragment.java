package com.dawson.ces.stocks.fragments;

import android.content.Context;
import android.content.SharedPreferences;
import android.os.AsyncTask;
import android.support.annotation.NonNull;

import android.os.Bundle;
import android.support.annotation.Nullable;
import android.support.v4.app.Fragment;
import android.text.TextUtils;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.EditText;
import android.widget.ProgressBar;
import android.widget.TextView;

import com.dawson.ces.feces.R;
import com.dawson.ces.stocks.animations.LoginProgress;
import com.dawson.ces.stocks.api.PortfolioAPI;
import com.dawson.ces.stocks.api.TokenManager;

import org.json.JSONObject;

/**
 * A login screen that offers login via email/password.
 *
 * @author Yanik
 */
public class LoginFragment extends Fragment {

    //================================================================================
    // Properties
    //================================================================================

    private EditText emailView;
    private EditText passwordView;
    private LoginProgress loginProgress;
    private TextView loginErrorView;
    private String email;
    private String password;

    //================================================================================
    // Lifecycle methods
    //================================================================================

    @Override
    public void onCreate(@Nullable Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);

        // Verify if email and password are in the shared preferences. If yes, it means
        // the user has already logged in and the portfolio should open without asking the
        // user to log in.
        SharedPreferences prefs =
                getContext().getSharedPreferences(getString(R.string.prefs_settings), Context.MODE_PRIVATE);
        this.email = prefs.getString(getString(R.string.prefs_email), "");
        this.password = prefs.getString(getString(R.string.prefs_password), "");
    }

    @Override
    public View onCreateView(LayoutInflater inflater, ViewGroup container,
                             Bundle savedInstanceState) {
        View view = inflater.inflate(R.layout.stocks_login, container, false);
        return view;
    }

    @Override
    public void onViewCreated(@NonNull View view, Bundle savedInstanceState) {
        super.onViewCreated(view, savedInstanceState);

        // Finding all necessary views
        emailView = view.findViewById(R.id.email);
        passwordView = view.findViewById(R.id.password);
        View emailSignInButton = view.findViewById(R.id.email_sign_in_button);
        View loginFormView = view.findViewById(R.id.login_form);
        loginErrorView = view.findViewById(R.id.login_error);
        ProgressBar progressView = view.findViewById(R.id.login_progress);

        // Creating the LoginProgress object which will display the login progress
        loginProgress = new LoginProgress(getContext(), progressView, loginFormView);

        // Setting the sign in button listener
        emailSignInButton.setOnClickListener(CreateSignInClickListener());
    }

  @Override
  public void onActivityCreated(@Nullable Bundle savedInstanceState) {
    super.onActivityCreated(savedInstanceState);

    // Attempt to login the user if there's an email & a username specified in the login
    if (!email.isEmpty() && !password.isEmpty()) {
      loginProgress.showProgress();
      UserLoginTask userLoginTask = new UserLoginTask();
      userLoginTask.execute();
    }
  }

  //================================================================================
    // Click Listeners
    //================================================================================

    private View.OnClickListener CreateSignInClickListener() {
        return (view) -> {
            attemptLogin();
        };
    }

    //================================================================================
    // Helper Methods
    //================================================================================

    /**
     * Attempts to sign in or register the account specified by the login form.
     * If there are form errors (invalid email, missing fields, etc.), the
     * errors are presented and no actual login attempt is made.
     */
    private void attemptLogin() {
        // Reset errors.
        emailView.setError(null);
        passwordView.setError(null);

        // Store values at the time of the login attempt.
        email = emailView.getText().toString();
        password = passwordView.getText().toString();

        boolean cancel = false;
        View focusView = null;

        // Check that password field is not empty
        if (TextUtils.isEmpty(password)) {
            passwordView.setError(getContext().getString(R.string.error_field_required));
            focusView = passwordView;
            cancel = true;
        }

        // Check that email field is not empty
        if (TextUtils.isEmpty(email)) {
            emailView.setError(getContext().getString(R.string.error_field_required));
            focusView = emailView;
            cancel = true;
        }

        if (cancel) {
            // There was an error; don't attempt login and focus the first
            // form field with an error.
            focusView.requestFocus();
        } else {
            // Show a progress spinner, and kick off a background task to
            // perform the user login attempt.
            loginProgress.showProgress();
            UserLoginTask userLoginTask = new UserLoginTask();
            userLoginTask.execute();
        }
    }

    //================================================================================
    // Async Tasks
    //================================================================================

    /**
     * Represents an asynchronous login/registration task used to authenticate
     * the user.
     */
    private class UserLoginTask extends AsyncTask<Void, Void, JSONObject> {
        @Override
        protected JSONObject doInBackground(Void... params) {
            return PortfolioAPI.login(email, password);
        }

        @Override
        protected void onPostExecute(JSONObject response) {
            String token = response == null ? null : response.optString("access_token");
            int expiresIn = response == null ? 0 : response.optInt("expires_in");


            loginProgress.hideProgress();

            if (response == null || TextUtils.isEmpty(token)) {
                loginErrorView.setText(R.string.stocks_general_login_error);
            } else {
                TokenManager tokenManager = new TokenManager(getContext());
                tokenManager.storeToken(token, expiresIn);

                // Open the portfolio
                PortfolioPagerFragment portfolioPagerFragment = new PortfolioPagerFragment();
                getFragmentManager().beginTransaction()
                        .addToBackStack(null)
                        .replace(R.id.content_frame, portfolioPagerFragment)
                        .commit();
            }
        }
    }
}

