package com.dawson.ces.stocks.fragments;

import android.os.AsyncTask;
import android.os.Bundle;
import android.support.annotation.NonNull;
import android.support.v4.app.Fragment;
import android.support.v7.widget.DividerItemDecoration;
import android.support.v7.widget.LinearLayoutManager;
import android.support.v7.widget.RecyclerView;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.TextView;

import com.dawson.ces.feces.R;
import com.dawson.ces.stocks.adapters.PortfolioListAdapter;
import com.dawson.ces.stocks.data.PortfolioItem;
import com.dawson.ces.stocks.data.Stock;
import com.dawson.ces.stocks.repositories.PortfolioRepository;
import com.dawson.ces.stocks.repositories.StockRepository;

import java.util.ArrayList;
import java.util.Arrays;
import java.util.List;

/**
 * The MyPortfolioFragment displays the user's balance and the his portfolio in a list
 *
 * @author Yanik
 */
public class MyPortfolioFragment extends Fragment {

    //================================================================================
    // Properties
    //================================================================================

    private List<PortfolioItem> portfolio;
    private double balance;
    private TextView balanceView ;
    private PortfolioListAdapter portfolioListAdapter;

    //================================================================================
    // Lifecycle Methods
    //================================================================================

    @Override
    public View onCreateView(LayoutInflater inflater, ViewGroup container,
                             Bundle savedInstanceState) {
        View view = inflater.inflate(R.layout.stocks_myportfolio_page, container, false);
        return view;
    }

    @Override
    public void onViewCreated(@NonNull View view, Bundle savedInstanceState) {
        super.onViewCreated(view, savedInstanceState);

        // Initializing variables
        portfolio = new ArrayList<>();

        // Finding all necessary views
        balanceView = view.findViewById(R.id.balance);
        RecyclerView portfolioRecyclerView = view.findViewById(R.id.my_stocks_list);

        // Setting up the RecyclerView
        portfolioListAdapter = new PortfolioListAdapter(getContext(), getActivity().getSupportFragmentManager(), portfolio);
        LinearLayoutManager layoutManager = new LinearLayoutManager(getContext());
        DividerItemDecoration dividerItemDecoration = new DividerItemDecoration(getContext(),
                layoutManager.getOrientation());
        portfolioRecyclerView.setAdapter(portfolioListAdapter);
        portfolioRecyclerView.setLayoutManager(layoutManager);
        portfolioRecyclerView.addItemDecoration(dividerItemDecoration);

        // Starting the AsyncTasks to fetch the portfolio and the balance
        LoadPortfolioTask loadPortfolioTask = new LoadPortfolioTask();
        loadPortfolioTask.execute();
        LoadBalanceTask loadBalanceTask = new LoadBalanceTask();
        loadBalanceTask.execute();
    }

    //================================================================================
    // Async Tasks
    //================================================================================

    /**
     * The LoadPortfolioItemsTask is responsible to load the portfolio
     */
    private class LoadPortfolioTask extends AsyncTask<Void, Void, PortfolioItem[]> {
        @Override
        protected PortfolioItem[] doInBackground(Void... voids) {
            // Getting the portfolio items
            PortfolioRepository portfolioRepository = new PortfolioRepository(getContext());
            PortfolioItem[] values = portfolioRepository.getPortfolioItems();
            StockRepository stockRepository = new StockRepository();
            List<Stock> userStocks = new ArrayList<>();
            //Get all stocks to be able to update them with proper values from our API
            for(PortfolioItem val : values)
                userStocks.add(val.getStock());
            stockRepository.updateStocks(userStocks);
            return values;
        }

        @Override
        protected void onPostExecute(PortfolioItem[] portfolioItems) {
            portfolio.clear();
            portfolio.addAll(Arrays.asList(portfolioItems));
            portfolioListAdapter.notifyDataSetChanged();
        }
    }

    private class LoadBalanceTask extends AsyncTask<Void, Void, Double> {
        @Override
        protected Double doInBackground(Void... voids) {
            // Getting the balance
            PortfolioRepository portfolioRepository = new PortfolioRepository(getContext());
            return portfolioRepository.getBalance();
        }

        @Override
        protected void onPostExecute(Double retrievedBalance) {
            balance = retrievedBalance;
            balanceView.setText(String.valueOf(balance));
        }
    }
}
