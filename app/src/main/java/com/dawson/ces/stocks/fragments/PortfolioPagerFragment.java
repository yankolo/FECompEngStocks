package com.dawson.ces.stocks.fragments;

import android.os.Bundle;
import android.support.annotation.NonNull;
import android.support.design.widget.TabLayout;
import android.support.v4.app.Fragment;
import android.support.v4.view.ViewPager;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import com.dawson.ces.feces.R;
import com.dawson.ces.stocks.adapters.PortfolioPagerAdapter;

/**
 * The only purpose of the PortfolioPagerFragment is to provide a tabbed layout where
 * the user can easily switch between his portfolio or the stock search.
 *
 * @author Yanik
 */
public class PortfolioPagerFragment extends Fragment {

    //================================================================================
    // Lifecycle Methods
    //================================================================================

    @Override
    public View onCreateView(@NonNull LayoutInflater inflater, ViewGroup container,
                             Bundle savedInstanceState) {
        return inflater.inflate(R.layout.stocks_pager, container, false);
    }

    @Override
    public void onViewCreated(@NonNull View view, Bundle savedInstanceState) {
        super.onViewCreated(view, savedInstanceState);

        // Get the ViewPager and set it's PagerAdapter so that it can display items
        ViewPager viewPager = view.findViewById(R.id.portfolio_viewpager);
        viewPager.setAdapter(new PortfolioPagerAdapter(getChildFragmentManager(),
                getActivity()));

        // Give the TabLayout the ViewPager
        TabLayout tabLayout = view.findViewById(R.id.sliding_tabs);
        tabLayout.setupWithViewPager(viewPager);
    }

}
