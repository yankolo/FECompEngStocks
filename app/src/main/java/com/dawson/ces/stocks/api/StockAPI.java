package com.dawson.ces.stocks.api;

import com.dawson.ces.util.HttpRequest;
import com.dawson.ces.util.HttpResponse;

import org.json.JSONException;
import org.json.JSONObject;

/**
 * The StockAPI provides several static methods to interact with a stock market API. All the
 * methods return a response in JSON objects.
 *
 * @author Yanik
 */
public class StockAPI {

    //================================================================================
    // Properties
    //================================================================================

    private final static String URL = "https://www.worldtradingdata.com/api/v1/stock";
    private final static String API_KEY = "O2TFy49QcMSKJIKvnd0wBvxvZ7Cg8UKLu3r7haDkQqWm7wGIQxjNYtdTgmgf";

    //================================================================================
    // Public Methods
    //================================================================================

    public static JSONObject search(String... symbols) {
        JSONObject jsonResponse = null;
        try {
            // Constructing the URL that will be used to make the API call
            String params = "symbol=" + implode(",", symbols) + "&api_token=" + API_KEY;
            String call = URL + "?" + params;

            // Making HTTP GET request
            HttpRequest http = new HttpRequest();
            http.setMethod("GET");
            http.setUrl(call);

            HttpResponse httpResponse = http.execute();
            jsonResponse = new JSONObject(httpResponse.getBody());
        } catch (JSONException e) {
            e.printStackTrace();
        }

        return jsonResponse;
    }

    //================================================================================
    // Helper Methods
    //================================================================================

    private static String implode(String glue, String[] pieces) {
        StringBuilder stringBuilder = new StringBuilder();

        // Appending comma after every piece, except the last one
        for (int i = 0; i < pieces.length; i++) {
            stringBuilder.append(pieces[i]);
            if (i < pieces.length - 1)
                stringBuilder.append(glue);
        }

        return stringBuilder.toString();
    }
}
