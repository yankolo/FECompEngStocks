package com.dawson.ces.stocks.api;

import com.dawson.ces.util.HttpRequest;
import com.dawson.ces.util.HttpResponse;

import org.json.JSONArray;
import org.json.JSONException;
import org.json.JSONObject;

/**
 * The PortfolioAPI provides several static methods to interact with the portfolio web
 * end point (in our case, our Heroku site). All the PortfolioAPI methods return
 * a response in JSON objects.
 *
 * @author Yanik
 */
public class PortfolioAPI {

    //================================================================================
    // Properties
    //================================================================================

    private final static String URL = "https://engstocks.herokuapp.com";

    //================================================================================
    // Public Methods
    //================================================================================

    public static JSONObject login(String email, String password) {
        JSONObject jsonResponse = null;
        try {
            HttpRequest http = new HttpRequest();
            http.setMethod("POST");
            http.appendHeader("Accept", "application/json");
            http.appendHeader("Content-Type", "application/x-www-form-urlencoded");
            http.setUrl(URL + "/api/auth/login");
            http.setBody("email=" + email + "&password=" + password);
            HttpResponse httpResponse = http.execute();

            jsonResponse =  new JSONObject(httpResponse.getBody());
        } catch (JSONException e) {
            e.printStackTrace();
        }

        return jsonResponse;
    }

    public static JSONArray requestAllStocks(String userToken) {
        JSONArray jsonResponse = null;
        try {
            HttpRequest http = new HttpRequest();
            http.setMethod("GET");
            http.appendHeader("Authorization", "Bearer " + userToken);
            http.appendHeader("Accept", "application/json");
            http.setUrl(URL + "/api/v1/allstocks");
            HttpResponse httpResponse = http.execute();

            jsonResponse = new JSONArray(httpResponse.getBody());
        } catch (JSONException e) {
            e.printStackTrace();
        }

        return jsonResponse;
    }

    public static JSONObject requestBalance(String userToken) {
        JSONObject jsonResponse = null;
        try {
            HttpRequest http = new HttpRequest();
            http.setMethod("GET");
            http.appendHeader("Authorization", "Bearer " + userToken);
            http.appendHeader("Accept", "application/json");
            http.setUrl(URL + "/api/v1/cash");
            HttpResponse httpResponse = http.execute();

            jsonResponse = new JSONObject(httpResponse.getBody());
        } catch (JSONException e) {
            e.printStackTrace();
        }

        return jsonResponse;
    }

    public static JSONObject requestBuyTransaction(String userToken, String ticker, int quantity) {
        JSONObject jsonResponse = null;
        try {
            HttpRequest http = new HttpRequest();
            http.setMethod("POST");
            http.appendHeader("Authorization", "Bearer " + userToken);
            http.appendHeader("Accept", "application/json");
            http.appendHeader("Content-Type", "application/json");
            http.setUrl(URL + "/api/v1/buy");

            // Constructing the json body
            JSONObject json = new JSONObject();
            json.put("ticker", ticker);
            json.put("quantity", quantity);

            http.setBody(json.toString());
            HttpResponse httpResponse = http.execute();
            jsonResponse = new JSONObject(httpResponse.getBody());
        } catch (JSONException e) {
            e.printStackTrace();
        }

        return jsonResponse;
    }

    public static JSONObject requestSellTransaction(String userToken, String ticker, int quantity) {
        JSONObject jsonResponse = null;
        try {
            HttpRequest http = new HttpRequest();
            http.setMethod("POST");
            http.appendHeader("Authorization", "Bearer " + userToken);
            http.appendHeader("Accept", "application/json");
            http.appendHeader("Content-Type", "application/json");
            http.setUrl(URL + "/api/v1/sell");

            // Constructing the json body
            JSONObject json = new JSONObject();
            json.put("ticker", ticker);
            json.put("quantity", quantity);

            http.setBody(json.toString());

            HttpResponse httpResponse = http.execute();
            jsonResponse = new JSONObject(httpResponse.getBody());
        } catch (JSONException e) {
            e.printStackTrace();
        }

        return jsonResponse;
    }
}
