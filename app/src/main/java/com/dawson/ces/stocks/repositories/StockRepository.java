package com.dawson.ces.stocks.repositories;

import com.dawson.ces.stocks.api.StockAPI;
import com.dawson.ces.stocks.data.Stock;

import org.json.JSONArray;
import org.json.JSONException;
import org.json.JSONObject;

import java.util.ArrayList;
import java.util.Collections;
import java.util.List;

/**
 * The StockRepository provides a data source abstraction for the stock market data. Use
 * this class to get information on stocks from the market.
 *
 * In our case, the StockRepository uses the StockAPI behind the scene to
 * get its data.
 *
 * @author Yanik
 */
public class StockRepository {

    //================================================================================
    // Public Methods
    //================================================================================

    /**
     * Given a list of symbols, gets a list of symbols
     *
     * @param symbols The stock ticker symbols
     * @return A list of the stocks that were found
     */
    public List<Stock> getStocks(String... symbols) {
        JSONObject json = StockAPI.search(symbols);
        return toStocks(json);
    }

    /**
     * Updates all the stocks in the list. The symbols of the stocks need to be
     * set. All the information in the stock objects will be updated (apart from the symbol itself).
     */
    public void updateStocks(List<Stock> stocks) {
        // Construct the list of symbols to make a call to the API
        Collections.sort(stocks,(Stock symbolA, Stock symbolB) -> symbolA.getSymbol().compareTo(symbolB.getSymbol()));
        List<String> symbols = new ArrayList<>();
        for (Stock stock: stocks) {
            symbols.add(stock.getSymbol());
        }


        JSONObject json = StockAPI.search(symbols.toArray(new String[0]));
        List<Stock> newStocks = toStocks(json);

        // Updating the stocks passed as parameters with the information contained in the
        // newly retrieved stock objects
        // Setting up 2 counters to manage the parallel lists (some stocks might not have been found)
        int i = 0; // Counter for the stocks that were initially passed in the list
        int j = 0; // Counter for the newly retrieved stocks

        while (i < stocks.size()) {
            // If the symbols are not equal, it means that the stock was not found
            // therefore, increasing the counter for the first list (i)
            if (!stocks.get(i).getSymbol().equals(newStocks.get(j).getSymbol())) {
                i++;
                continue;
            }

            Stock stockToUpdate = stocks.get(i);
            Stock newStock = newStocks.get(j);

            // Updating the stock
            stockToUpdate.setCompanyName(newStock.getCompanyName());
            stockToUpdate.setPrice(newStock.getPrice());
            stockToUpdate.setChangePercentage(newStock.getChangePercentage());
            stockToUpdate.setCurrency(newStock.getCurrency());
            stockToUpdate.setStockExchange(newStock.getStockExchange());

            i++;
            j++;
        }
    }

    //================================================================================
    // Helper Methods
    //================================================================================

    private List<Stock> toStocks(JSONObject json) {
        List<Stock> stocks = new ArrayList<>();

        try {
            JSONArray data = json.getJSONArray("data");

            for (int i = 0; i < data.length(); i++) {
                JSONObject stockData = data.getJSONObject(i);

                Stock stock = new Stock();
                stock.setCompanyName(stockData.getString("name"));
                stock.setSymbol(stockData.getString("symbol"));
                stock.setPrice(Double.parseDouble(stockData.getString("price")));
                stock.setCurrency(stockData.getString("currency"));
                stock.setChangePercentage(stockData.getDouble("change_pct"));
                stock.setStockExchange(stockData.getString("stock_exchange_short"));

                stocks.add(stock);
            }
        } catch (JSONException e) {
            e.printStackTrace();
        }

        return stocks;
    }
}
