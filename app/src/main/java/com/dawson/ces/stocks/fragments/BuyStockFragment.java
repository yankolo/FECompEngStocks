package com.dawson.ces.stocks.fragments;

import android.content.Context;
import android.os.AsyncTask;
import android.os.Bundle;
import android.support.annotation.NonNull;
import android.support.annotation.Nullable;
import android.support.design.widget.Snackbar;
import android.support.v4.app.Fragment;
import android.support.v4.app.FragmentTransaction;
import android.text.method.KeyListener;
import android.view.KeyEvent;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.Button;
import android.widget.EditText;
import android.widget.TextView;

import com.dawson.ces.feces.R;
import com.dawson.ces.stocks.data.PortfolioItem;
import com.dawson.ces.stocks.data.Stock;
import com.dawson.ces.stocks.exceptions.BuyException;
import com.dawson.ces.stocks.repositories.PortfolioRepository;
import com.dawson.ces.stocks.repositories.StockRepository;

import static android.support.design.widget.Snackbar.LENGTH_SHORT;

/**
 * The BuyStockFragment represents the fragment that is displayed when the "Buy" button
 * is pressed in the StockDetailsFragment. The BuyStockFragment displays an input that
 * asks the user to input a quantity of stocks to buy. It performs validation on the input
 * prior to activating the "Confirm Buy" button.
 *
 * @author Yanik
 */
public class BuyStockFragment extends Fragment {

    //================================================================================
    // Properties
    //================================================================================

    private Stock stock;
    private double balance;
    private int quantityBuy;
    private EditText userInput;
    private Button buyButton;

    //================================================================================
    // Fragment Instantiation
    //================================================================================

    public static BuyStockFragment newInstance(Context context, Stock stock, double balance) {
        BuyStockFragment buyStockFragment = new BuyStockFragment();

        Bundle args = new Bundle();
        args.putSerializable("stock", stock);
        args.putDouble("balance", balance);
        buyStockFragment.setArguments(args);

        return buyStockFragment;
    }

    //================================================================================
    // Lifecycle Methods
    //================================================================================

    @Override
    public void onCreate(@Nullable Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        stock = (Stock) this.getArguments().getSerializable("stock");
        balance = this.getArguments().getDouble("balance");
    }

    @Override
    public View onCreateView(LayoutInflater inflater, ViewGroup container,
                             Bundle savedInstanceState) {
        View view = inflater.inflate(R.layout.stocks_buy, container, false);
        return view;
    }

    @Override
    public void onViewCreated(@NonNull View view, @Nullable Bundle savedInstanceState) {
        super.onViewCreated(view, savedInstanceState);

        // Finding all the necessary views
        buyButton = view.findViewById(R.id.buy_confirm_button);
        userInput = view.findViewById(R.id.quantity_input);
        TextView cashLeft = view.findViewById(R.id.cash_left);
        cashLeft.setText(String.valueOf(balance));

        // Assigning listeners
        buyButton.setOnClickListener(createBuyButtonClickListener());
        userInput.setOnKeyListener(createInputKeyListener(view));
    }

    //================================================================================
    // Listeners
    //================================================================================

    private View.OnClickListener createBuyButtonClickListener() {
        return (v) -> {
            quantityBuy = Integer.parseInt(userInput.getText().toString());
            BuyTask buyTask = new BuyTask();
            buyTask.execute();
        };
    }

    private View.OnKeyListener createInputKeyListener(View view) {
        return (v, keyCode, event) -> {
            char s = event.getDisplayLabel();
            if (event.getAction() != KeyEvent.ACTION_DOWN && keyCode == KeyEvent.KEYCODE_DEL) {
                if (!((EditText) v).getText().toString().isEmpty())
                    calculate(' ');
            }
            if (event.getAction() != KeyEvent.ACTION_DOWN)
                return true;
            if (keyCode == KeyEvent.KEYCODE_DEL) {
                if (((EditText) v).getText().toString().length() <= 1) {
                    TextView t = view.findViewById(R.id.cash_left);
                    t.setText(balance + "");
                    buyButton.setEnabled(false);
                }
            }
            if (Character.isDigit(s)) {
                calculate(s);
            }
            return false;
        };
    }

    //================================================================================
    // Helper Methods
    //================================================================================

    private void calculate(char s) {
        Button buy = getView().findViewById(R.id.buy_confirm_button);
        String input = ((EditText) getView().findViewById(R.id.quantity_input)).getText().toString();
        input += s;
        input =input.trim();
        double value = Double.parseDouble(input);
        double newBalance = this.balance - (value * stock.getPrice());
        double possibleBalance = (double) Math.round(newBalance * 100.0) / 100.0;
        TextView t = getView().findViewById(R.id.cash_left);
        if (possibleBalance < 0) {
            t.setText("Not Enough");
            buy.setEnabled(false);
        } else {
            t.setText(possibleBalance + "");
            buy.setEnabled(true);
        }
    }

    //================================================================================
    // Async Tasks
    //================================================================================

    private class BuyTask extends AsyncTask<Void, Void, Void> {
        private boolean errorWhileBuying;
        private String error;
        private double cashleft;

        @Override
        protected Void doInBackground(Void... voids) {
            PortfolioRepository portfolioRepository = new PortfolioRepository(getContext());

            try {
                cashleft = portfolioRepository.buyStock(stock.getSymbol(), quantityBuy);
            } catch (BuyException e) {
                errorWhileBuying = true;
                error = e.getMessage();
            }

            return null;
        }

        @Override
        protected void onPostExecute(Void aVoid) {
            FragmentTransaction transaction = getFragmentManager().beginTransaction();
            transaction.remove(BuyStockFragment.this);
            transaction.commit();

            if (errorWhileBuying) {
                Snackbar.make(getActivity().findViewById(android.R.id.content),
                        error, Snackbar.LENGTH_SHORT).show();
            } else {
                ((StockDetailsFragment) BuyStockFragment.this.getParentFragment()).updateBalance(cashleft);
                Snackbar.make(getActivity().findViewById(android.R.id.content),
                        R.string.buy_success_snackbar, Snackbar.LENGTH_SHORT).show();
            }
        }
    }
}
