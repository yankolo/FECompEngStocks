package com.dawson.ces.stocks.adapters;

import android.content.Context;
import android.support.v4.app.Fragment;
import android.support.v4.app.FragmentManager;
import android.support.v4.app.FragmentPagerAdapter;

import com.dawson.ces.stocks.fragments.MyPortfolioFragment;
import com.dawson.ces.stocks.fragments.StockSearchFragment;


/**
 * Adapter that is used to display tabs
 *
 * @author Yanik
 */
public class PortfolioPagerAdapter extends FragmentPagerAdapter {

    //================================================================================
    // Properties
    //================================================================================

    private final int PAGE_COUNT = 2;
    private String tabTitles[] = new String[] { "My Portfolio", "Search"};
    private Context context;

    //================================================================================
    // Constructors
    //================================================================================

    public PortfolioPagerAdapter(FragmentManager fragmentManager, Context context) {
        super(fragmentManager);
        this.context = context;
    }

    //================================================================================
    // FragmentPagerAdapter Overridden Methods
    //================================================================================

    @Override
    public int getCount() {
        return PAGE_COUNT;
    }

    @Override
    public Fragment getItem(int position) {
        switch (position) {
            case 0:
                return new MyPortfolioFragment();
            case 1:
                return new StockSearchFragment();
            default:
                throw new IllegalArgumentException("Illegal position");
        }
    }

    @Override
    public CharSequence getPageTitle(int position) {
        // Generate title based on item position
        return tabTitles[position];
    }
}
