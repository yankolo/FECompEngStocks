package com.dawson.ces.stocks.adapters;

import android.content.Context;
import android.support.annotation.NonNull;
import android.support.v4.app.FragmentManager;
import android.support.v4.app.FragmentTransaction;
import android.support.v7.widget.RecyclerView;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.TextView;

import com.dawson.ces.feces.R;
import com.dawson.ces.stocks.data.PortfolioItem;
import com.dawson.ces.stocks.data.Stock;
import com.dawson.ces.stocks.fragments.StockDetailsFragment;

import java.util.List;

/**
 * Adapter that is used to link a RecyclerView with PortfolioItems
 *
 * @author Yanik
 */
public class PortfolioListAdapter extends  RecyclerView.Adapter<RecyclerView.ViewHolder> {

    //================================================================================
    // View Holders
    //================================================================================

    public class PortfolioItemViewHolder extends RecyclerView.ViewHolder {
        TextView stockSymbol;
        TextView companyName;
        TextView stockPrice;
        TextView changePercentage;
        TextView quantity;


        PortfolioItemViewHolder(View view) {
            super(view);
            this.stockSymbol = (TextView) view.findViewById(R.id.stock_symbol);
            this.companyName = (TextView) view.findViewById(R.id.company_name);
            this.stockPrice = (TextView) view.findViewById(R.id.stock_price);
            this.changePercentage = (TextView) view.findViewById(R.id.stock_percentage_change);
            this.quantity = (TextView) view.findViewById(R.id.quantity);
        }
    }

    //================================================================================
    // Properties
    //================================================================================

    private Context context;
    private FragmentManager fragmentManager;
    private List<PortfolioItem> portfolioItems;

    //================================================================================
    // Constructors
    //================================================================================

    public PortfolioListAdapter(Context context, FragmentManager fragmentManager, List<PortfolioItem> portfolioItems) {
        this.context = context;
        this.fragmentManager = fragmentManager;
        this.portfolioItems = portfolioItems;
    }

    //================================================================================
    // RecyclerView Overridden Methods
    //================================================================================

    @NonNull
    @Override
    public RecyclerView.ViewHolder onCreateViewHolder(@NonNull ViewGroup viewGroup, int viewType) {
        LayoutInflater layoutInflater = LayoutInflater.from(viewGroup.getContext());

        View stockListItem = layoutInflater.inflate(R.layout.stocks_portfolio_item, viewGroup, false);
        return new PortfolioListAdapter.PortfolioItemViewHolder(stockListItem);
    }

    @Override
    public void onBindViewHolder(@NonNull RecyclerView.ViewHolder viewHolder, int position) {
        PortfolioItem itemToDisplay = portfolioItems.get(position);
        PortfolioListAdapter.PortfolioItemViewHolder portfolioItemViewHolder =
                (PortfolioListAdapter.PortfolioItemViewHolder) viewHolder;

        portfolioItemViewHolder.stockSymbol.setText(itemToDisplay.getStock().getSymbol());
        portfolioItemViewHolder.companyName.setText(itemToDisplay.getStock().getCompanyName());
        portfolioItemViewHolder.stockPrice.setText(String.valueOf(itemToDisplay.getStock().getPrice()));
        portfolioItemViewHolder.changePercentage.setText(String.valueOf(itemToDisplay.getStock().getChangePercentage()));
        portfolioItemViewHolder.quantity.setText(String.valueOf(itemToDisplay.getQuantity()));

        viewHolder.itemView.setOnClickListener(CreateItemClickListener(portfolioItems.get(position).getStock()));
    }

    @Override
    public int getItemCount() {
        return portfolioItems.size();
    }


    private View.OnClickListener CreateItemClickListener(Stock stock) {
        return (view) -> {
            StockDetailsFragment stockDetailsFragment = StockDetailsFragment.newInstance(context, stock);

            FragmentTransaction transaction = fragmentManager.beginTransaction();
            transaction.addToBackStack(null);
            transaction.replace(R.id.content_frame, stockDetailsFragment);

            transaction.commit();
        };
    }

    //================================================================================
    // Accessors
    //================================================================================

    public List<PortfolioItem> getPortfolioItems() {
        return portfolioItems;
    }
}
