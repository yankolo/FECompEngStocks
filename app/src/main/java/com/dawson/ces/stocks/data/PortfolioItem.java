package com.dawson.ces.stocks.data;

/**
 * A Portfolio Item represents one item in the portfolio. It contains a stock and a
 * quantity. A user could have many portfolio items, which forms a portfolio.
 *
 * @author Yanik
 */
public class PortfolioItem {

    //================================================================================
    // Properties
    //================================================================================

    private Stock stock;
    private int quantity;

    //================================================================================
    // Constructors
    //================================================================================

    public PortfolioItem() {
    }

    public PortfolioItem(Stock stock, int quantity) {
        this.stock = stock;
        this.quantity = quantity;
    }

    //================================================================================
    // Accessors
    //================================================================================

    public Stock getStock() {
        return stock;
    }

    public void setStock(Stock stock) {
        this.stock = stock;
    }

    public int getQuantity() {
        return quantity;
    }

    public void setQuantity(int quantity) {
        this.quantity = quantity;
    }
}
