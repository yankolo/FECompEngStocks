package com.dawson.ces.stocks.fragments;

import android.content.Context;
import android.os.AsyncTask;
import android.os.Bundle;
import android.support.annotation.NonNull;
import android.support.annotation.Nullable;
import android.support.v4.app.Fragment;
import android.support.v4.app.FragmentTransaction;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.Button;
import android.widget.TextView;

import com.dawson.ces.feces.R;
import com.dawson.ces.stocks.data.PortfolioItem;
import com.dawson.ces.stocks.data.Stock;
import com.dawson.ces.stocks.repositories.PortfolioRepository;
import com.dawson.ces.stocks.repositories.StockRepository;

import java.util.ArrayList;
import java.util.Arrays;
import java.util.List;

/**
 * The StockDetailsFragment displays detailed information about a given stock.
 * In addition to that, it provides two buttons: "Buy" and "Sell". Whenever the
 * user clicks on any of those buttons a fragment appears below the buttons that
 * will ask the user how many stocks he wants to buy/sell.
 *
 * @author Yanik
 */
public class StockDetailsFragment extends Fragment {

    //================================================================================
    // Properties
    //================================================================================

    private Stock stock;
    private double balance;
    private int quantityOwned;

    Button buyButton;
    Button sellButton;

    //================================================================================
    // Fragment Instantiation
    //================================================================================

    /**
     * Instantiate a new StockDetailsFragment.
     *
     * @param context The context
     * @param stock The stock to show details of
     * @return Returns an instance of the StockDetailsFragment
     */
    public static StockDetailsFragment newInstance(Context context, Stock stock) {
        StockDetailsFragment stockDetailsFragment = new StockDetailsFragment();

        Bundle args = new Bundle();
        args.putSerializable("stock", stock);
        stockDetailsFragment.setArguments(args);

        return stockDetailsFragment;
    }

    //================================================================================
    // Lifecycle Methods
    //================================================================================

    @Override
    public void onCreate(@Nullable Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        stock = (Stock) this.getArguments().getSerializable("stock");
    }

    @Override
    public View onCreateView(@NonNull LayoutInflater inflater, ViewGroup container,
                             Bundle savedInstanceState) {
        return inflater.inflate(R.layout.stocks_details, container, false);
    }

    @Override
    public void onViewCreated(View view, Bundle savedInstanceState) {
        super.onViewCreated(view, savedInstanceState);

        // Finding all necessary views
        TextView companyName = view.findViewById(R.id.company_name);
        TextView symbol = view.findViewById(R.id.stock_symbol);
        TextView currency = view.findViewById(R.id.currency);
        TextView price = view.findViewById(R.id.price);
        TextView changePercentage = view.findViewById(R.id.change_percentage);
        TextView stockExchange = view.findViewById(R.id.stock_exchange);
        buyButton = view.findViewById(R.id.buy_button);
        sellButton = view.findViewById(R.id.sell_button);
        ViewGroup transactionPlaceholder = view.findViewById(R.id.transaction_placeholder);

        // Assigning values to views
        companyName.setText(stock.getCompanyName());
        symbol.setText(stock.getSymbol());
        currency.setText(stock.getCurrency());
        price.setText(Double.toString(stock.getPrice()));
        changePercentage.setText(Double.toString(stock.getChangePercentage()));
        stockExchange.setText(stock.getStockExchange());

        // Creating and executing the AsyncTask that will load the balance
        GetBalanceAndQuantityTask getBalanceAndQuantityTask = new GetBalanceAndQuantityTask();
        getBalanceAndQuantityTask.execute();
    }

    //================================================================================
    // Click Listeners
    //================================================================================

    private View.OnClickListener createBuyClickListener() {
        return (view) -> {
            BuyStockFragment buyStockFragment = BuyStockFragment.newInstance(getContext(),
                    stock, balance);

            FragmentTransaction transaction = getChildFragmentManager().beginTransaction();
            transaction.replace(R.id.transaction_placeholder, buyStockFragment);
            transaction.commit();
        };
    }

    private View.OnClickListener createSellClickListener() {
        return (view) -> {
            SellStockFragment sellStockFragment = SellStockFragment.newInstance(getContext(),
                    stock, balance, quantityOwned);


            FragmentTransaction transaction = getChildFragmentManager().beginTransaction();
            transaction.replace(R.id.transaction_placeholder, sellStockFragment);
            transaction.commit();
        };
    }

    //================================================================================
    // Public Methods
    //================================================================================

    /**
     * Update the balance that is stored in this fragment. This method is called
     * from the child fragment whenever the buy/sell transaction modifies the balance
     */
    public void updateBalance(double balance) {
        this.balance = balance;
    }

    //================================================================================
    // Helper Methods
    //================================================================================

    /**
     * The transactions buttons are disabled by default. They are enabled
     * whenever all the necessary information is fetched from internet (e.g. balance)
     *
     * This method enables the transaction buttons.
     *
     * It also takes into account if the sell button needs to be enabled (it shouldn't be
     * enabled if stocksOwned is 0)
     */
    private void enableTransactionButtons() {
        buyButton.setOnClickListener(createBuyClickListener());
        buyButton.setEnabled(true);

        if (quantityOwned != 0) {
            sellButton.setOnClickListener(createSellClickListener());
            sellButton.setEnabled(true);
        }
    }

    //================================================================================
    // Async Tasks
    //================================================================================

    private class GetBalanceAndQuantityTask extends AsyncTask<Void, Void, Void> {
        @Override
        protected Void doInBackground(Void... voids) {
            // Getting the portfolio items
            PortfolioRepository portfolioRepository = new PortfolioRepository(getContext());
            PortfolioItem[] portfolioItems = portfolioRepository.getPortfolioItems();

            // Trying to find the stock in the portfolio to get the quantity the user owns
            // If he doesn't own the stock, the sell button will not be enabled
            boolean hasStock = false;
            for (PortfolioItem portfolioItem: portfolioItems) {
                if (stock.getSymbol().equals(portfolioItem.getStock().getSymbol())) {
                    quantityOwned = portfolioItem.getQuantity();
                    hasStock = true;
                }
            }

            // The user doesn't own the stock, set quantityOwned to 0
            if (!hasStock)
                quantityOwned = 0;

            // Getting the balance
            balance = portfolioRepository.getBalance();

            return null;
        }

        @Override
        protected void onPostExecute(Void aVoid) {
            enableTransactionButtons();
        }
    }
}
