package com.dawson.ces.stocks.exceptions;

public class BuyException extends Exception {
    public BuyException(String message) {
        super(message);
    }
}
