package com.dawson.ces.stocks.fragments;

import android.os.AsyncTask;
import android.os.Bundle;
import android.support.annotation.NonNull;
import android.support.design.chip.Chip;
import android.support.design.chip.ChipGroup;
import android.support.design.widget.Snackbar;
import android.support.v4.app.Fragment;
import android.support.v7.widget.DividerItemDecoration;
import android.support.v7.widget.LinearLayoutManager;
import android.support.v7.widget.RecyclerView;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.Button;
import android.widget.EditText;

import com.dawson.ces.feces.R;
import com.dawson.ces.stocks.adapters.StockListAdapter;
import com.dawson.ces.stocks.data.Stock;
import com.dawson.ces.stocks.repositories.StockRepository;

import java.util.ArrayList;
import java.util.List;

/**
 * The StockSearchFragment provides a way to search for stocks. The user can
 * specify several stocks to search at one time. The stocks that are found are
 * displayed in a RecyclerView.
 *
 * @author Yanik
 */
public class StockSearchFragment extends Fragment {

    //================================================================================
    // Properties
    //================================================================================

    private StockListAdapter stockListAdapter;
    private EditText symbolInput;
    private ChipGroup chipsStocksToSearch;
    private List<String> stocksToSearch;

    //================================================================================
    // Lifecycle Methods
    //================================================================================

    @Override
    public View onCreateView(@NonNull LayoutInflater inflater, ViewGroup container,
                             Bundle savedInstanceState) {
        return inflater.inflate(R.layout.stocks_search_page, container, false);
    }

    @Override
    public void onViewCreated(View view, Bundle savedInstanceState) {
        super.onViewCreated(view, savedInstanceState);

        // Initializing variables
        stocksToSearch = new ArrayList<>();
        List<Stock> stocksInList = new ArrayList<>();

        // Finding all necessary views
        symbolInput = view.findViewById(R.id.symbol_input);
        chipsStocksToSearch = view.findViewById(R.id.symbols_to_search);
        Button addToSearchButton = view.findViewById(R.id.add_to_search_button);
        Button searchButton = view.findViewById(R.id.search_button);
        Button clearButton = view.findViewById(R.id.clear_button);
        RecyclerView stocksRecyclerView = view.findViewById(R.id.stock_recycler_view);

        // Setting up the RecyclerView
        stockListAdapter = new StockListAdapter(getContext(), getActivity().getSupportFragmentManager(), stocksInList);
        LinearLayoutManager layoutManager = new LinearLayoutManager(getContext());
        DividerItemDecoration dividerItemDecoration = new DividerItemDecoration(getContext(),
                layoutManager.getOrientation());
        stocksRecyclerView.setAdapter(stockListAdapter);
        stocksRecyclerView.setLayoutManager(layoutManager);
        stocksRecyclerView.addItemDecoration(dividerItemDecoration);

        // Assigning click listeners to buttons
        addToSearchButton.setOnClickListener(createAddToSearchClickListener());
        searchButton.setOnClickListener(createSearchClickListener());
        clearButton.setOnClickListener(createClearClickListener());
    }

    //================================================================================
    // Click Listeners
    //================================================================================

    private View.OnClickListener createAddToSearchClickListener() {
        return (view) -> {
            String symbol = symbolInput.getText().toString().trim();

            // Add symbol to the stocks that need to be searched only if the input is not empty
            if (symbol.length() != 0) {
                stocksToSearch.add(symbol);
                chipsStocksToSearch.addView(createChip(symbol));
            }
        };
    }

    private View.OnClickListener createSearchClickListener() {
        return (view) -> {
            // Creating the AsyncTask that will query the stock API
            SearchStocksTask searchStocksTask = new SearchStocksTask();
            searchStocksTask.execute(stocksToSearch.toArray(new String[0]));
        };
    }

    private View.OnClickListener createClearClickListener() {
        return (view) -> {
            stocksToSearch.clear();
            chipsStocksToSearch.removeAllViews();

            stockListAdapter.getStocks().clear();
            stockListAdapter.notifyDataSetChanged();
        };
    }

    //================================================================================
    // Helper Methods
    //================================================================================

    private Chip createChip(String text) {
        LayoutInflater inflater = LayoutInflater.from(getContext());

        Chip chip = (Chip) inflater.inflate(R.layout.stocks_search_chip, null);
        chip.setText(text);

        return chip;
    }

    //================================================================================
    // Async Tasks
    //================================================================================

    private class SearchStocksTask extends AsyncTask<String, Void, List<Stock>> {

        @Override
        protected List<Stock> doInBackground(String... symbols) {
            StockRepository stockRepository = new StockRepository();
            return stockRepository.getStocks(symbols);
        }

        @Override
        protected void onPostExecute(List<Stock> stocks) {
            stockListAdapter.getStocks().clear();
            stockListAdapter.getStocks().addAll(stocks);
            stockListAdapter.notifyDataSetChanged();

            // If the stocksToSearch list size is not equal to the number of stocks found
            // pop up a snack bar that says that that some stocks were not found
            if (stocksToSearch.size() != stocks.size()) {
                Snackbar.make(getActivity().findViewById(android.R.id.content),
                        R.string.snackbar_some_stocks_not_found, Snackbar.LENGTH_LONG).show();
            }
        }
    }
}
