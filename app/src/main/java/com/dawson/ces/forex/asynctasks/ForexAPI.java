package com.dawson.ces.forex.asynctasks;

import android.os.AsyncTask;
import android.widget.TextView;

import com.dawson.ces.util.HttpRequest;
import com.dawson.ces.util.HttpResponse;

import org.json.JSONException;
import org.json.JSONObject;

import java.util.ArrayList;
import java.util.List;

public class ForexAPI extends AsyncTask<String, Void, List<String>> {

    private String key = "408c20a51aa75a3b2d2ba489c0bd33f7";
    private TextView tvt;
    private String userCurrecy;
    private String toCurrency;
    private String initialAmount;

    public ForexAPI(TextView tvt) {
        this.tvt = tvt;
    }

    private double[] getForex() {
        HttpRequest http = new HttpRequest();
        http.setMethod("GET");
        http.setUrl("http://data.fixer.io/api/latest?access_key=" + key);
        HttpResponse httpResponse = http.execute();
        return getRateAndResponse(httpResponse.getBody());
    }


    private double[] getRateAndResponse(String json) {
        try {
            JSONObject jsonObject = new JSONObject(json);
            JSONObject jsonObject1 = jsonObject.getJSONObject("rates");
            double rate = jsonObject1.getDouble(userCurrecy) / jsonObject1.getDouble(toCurrency);
            double value = Double.parseDouble(initialAmount);
            // We can't use NumberFormat until API 24
            double fo = Math.floor(rate * value * 100) / 100;
            return new double[]{rate, fo};
        } catch (JSONException e) {
            e.printStackTrace();
        }
        return null;
    }

    @Override
    protected List<String> doInBackground(String... strings) {
        List<String> s = new ArrayList<>();
        this.userCurrecy = strings[0];
        this.toCurrency = strings[1];
        this.initialAmount = strings[2];
        double[] d = getForex();
        for (double v : d) {
            s.add(Double.toString(v));
        }
        return s;
    }

    @Override
    protected void onPostExecute(List<String> strings) {
        tvt.setText(strings.get(1));
    }
}
