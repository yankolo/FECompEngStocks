package com.dawson.ces.forex.asynctasks;

import android.content.Context;
import android.os.AsyncTask;
import android.widget.ArrayAdapter;
import android.widget.Spinner;

import com.dawson.ces.util.HttpRequest;
import com.dawson.ces.util.HttpResponse;

import org.json.JSONException;
import org.json.JSONObject;

import java.util.ArrayList;
import java.util.Iterator;
import java.util.List;

public class CurrencyAPI extends AsyncTask<Void, Void, List<String>> {

    private String key = "408c20a51aa75a3b2d2ba489c0bd33f7";
    private Spinner spTo;
    private Spinner spFrom;
    private Context context;
    private String defaultTo;

    public CurrencyAPI(Spinner spTo, Spinner spFrom, Context context, String defaultTo) {
        this.spTo = spTo;
        this.spFrom = spFrom;
        this.context = context;
        this.defaultTo = defaultTo;
    }


    private List<String> getCurrencies() {
        HttpRequest http = new HttpRequest();
        http.setMethod("GET");
        http.setUrl("http://data.fixer.io/api/latest?access_key=" + key);
        HttpResponse httpResponse = http.execute();
        return getCurrencyName(httpResponse.getBody());
    }

    private List<String> getCurrencyName(String json) {
        try {
            List<String> ls = new ArrayList<>();
            JSONObject js = new JSONObject(json);
            JSONObject js1 = js.getJSONObject("rates");
            Iterator<String> st = js1.keys();
            while (st.hasNext()) {
                String key = st.next();
                ls.add(key);
            }
            return ls;
        } catch (JSONException e) {
            e.printStackTrace();
        }
        return null;
    }


    @Override
    protected List<String> doInBackground(Void... voidObj) {
        return getCurrencies();
    }

    @Override
    protected void onPostExecute(List<String> strings) {
        String[] s = new String[strings.size()];
        s = strings.toArray(s);
        ArrayAdapter<String> adapter =
                new ArrayAdapter<>(context, android.R.layout.simple_spinner_item, s);

        adapter.setDropDownViewResource(android.R.layout.simple_spinner_dropdown_item);

        //set the spinners adapter to the previously created one.
        spTo.setAdapter(adapter);
        spFrom.setAdapter(adapter);

        // Set the "To" currency to the default one the user specified
        spTo.setSelection(adapter.getPosition(defaultTo));
    }
}
