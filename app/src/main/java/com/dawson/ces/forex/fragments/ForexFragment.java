package com.dawson.ces.forex.fragments;

import android.content.SharedPreferences;
import android.os.Bundle;
import android.support.annotation.NonNull;
import android.support.annotation.Nullable;
import android.support.design.widget.Snackbar;
import android.support.v4.app.Fragment;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.Adapter;
import android.widget.ArrayAdapter;
import android.widget.Button;
import android.widget.EditText;
import android.widget.Spinner;
import android.widget.TextView;

import com.dawson.ces.feces.R;
import com.dawson.ces.forex.asynctasks.CurrencyAPI;
import com.dawson.ces.forex.asynctasks.ForexAPI;
import com.dawson.ces.settings.enums.Currency;

import java.util.ArrayList;
import java.util.List;

import static android.content.Context.MODE_PRIVATE;


public class ForexFragment extends Fragment {

    private String userInput = "";
    private String fromCurrency;
    private String toCurrency;
    private String result = "";
    private String defaultVal;
    private String[] currencies;

    @Nullable
    @Override
    public View onCreateView(@NonNull LayoutInflater inflater, @Nullable ViewGroup container,
                             @Nullable Bundle savedInstanceState) {
        return inflater.inflate(R.layout.forex, container, false);
    }

    @Override
    public void onViewCreated(@NonNull View view, @Nullable Bundle savedInstanceState) {
        super.onViewCreated(view, savedInstanceState);
    if(savedInstanceState==null) {
        SharedPreferences prefs = getActivity()
                .getSharedPreferences(getString(R.string.prefs_settings), MODE_PRIVATE);

        Currency userDefaultCurrency =
                Currency.fromId(prefs.getInt(getString(R.string.prefs_currency), 0));

        CurrencyAPI api =
                new CurrencyAPI(
                        getView().findViewById(R.id.toSpinner),
                        getView().findViewById(R.id.userSpinner),
                        getView().getContext(),
                        userDefaultCurrency.getSymbol());

        api.execute();
    }else{
        setSpinner(savedInstanceState.getStringArray("currencies"));

        setSelectItemOfSpinner(((Spinner) getView().findViewById(R.id.toSpinner)),savedInstanceState.getString("toCurrency"));
        setSelectItemOfSpinner(((Spinner) getView().findViewById(R.id.userSpinner)),savedInstanceState.getString("userCurrency"));
        ((EditText) getView().findViewById(R.id.userInput)).setText(savedInstanceState.getString("userInputMoney"));
        ((TextView) getView().findViewById(R.id.display)).setText(savedInstanceState.getString("result"));

    }

        Button button = view.findViewById(R.id.calculateBtn);

        button.setOnClickListener(v -> {
            ForexAPI a = new ForexAPI(view.findViewById(R.id.display));

            String input = ((EditText) view.findViewById(R.id.userInput)).getText().toString();
            double fromAmount;

            try {
                fromAmount = Double.parseDouble(input);
            } catch (NumberFormatException e) {
                Snackbar
                        .make(view, R.string.not_a_valid_number, Snackbar.LENGTH_SHORT)
                        .show();
                return;
            }

            a.execute(
                    ((Spinner) view.findViewById(R.id.toSpinner)).getSelectedItem().toString(),
                    ((Spinner) view.findViewById(R.id.userSpinner)).getSelectedItem().toString(),
                    Double.toString(fromAmount)
            );
        });
    }

    @Override
    public void onSaveInstanceState(@NonNull Bundle outState) {
        super.onSaveInstanceState(outState);
        outState.putString("userInputMoney", userInput);
        outState.putString("result", result);
        outState.putString("toCurrency", toCurrency);
        outState.putString("userCurrency", fromCurrency);
        outState.putStringArray("currencies",currencies);
    }




    @Override
    public void onPause() {
        super.onPause();
        saveValueOfSpinner(((Spinner) getView().findViewById(R.id.userSpinner)));
        userInput = ((EditText) getView().findViewById(R.id.userInput)).getText().toString();
        result = ((TextView) getView().findViewById(R.id.display)).getText().toString();
        toCurrency = ((Spinner) getView().findViewById(R.id.toSpinner)).getSelectedItem().toString();
        fromCurrency = ((Spinner) getView().findViewById(R.id.userSpinner)).getSelectedItem().toString();
    }

    private void setSelectItemOfSpinner(Spinner spinner,String value){
        for(int i = 0 ; i < spinner.getCount();i++){
            if(spinner.getItemAtPosition(i).equals(value)) {
                spinner.setSelection(i);
                break;
            }
        }
    }
    private void saveValueOfSpinner(Spinner spinner){
        List<String> currencySaved = new ArrayList<>();
        for(int i = 0 ; i < spinner.getCount();i++){
            currencySaved.add((String) spinner.getItemAtPosition(i));
        }
        currencies =  new String[currencySaved.size()];
        currencies = currencySaved.toArray(currencies);
    }


    private void setSpinner(String[] currencies){

        ArrayAdapter<String> adapter =
                new ArrayAdapter<>(getView().getContext(), android.R.layout.simple_spinner_item, currencies);
        adapter.setDropDownViewResource(android.R.layout.simple_spinner_dropdown_item);
        ((Spinner) getView().findViewById(R.id.userSpinner)).setAdapter(adapter);
        ((Spinner) getView().findViewById(R.id.toSpinner)).setAdapter(adapter);

    }

}
