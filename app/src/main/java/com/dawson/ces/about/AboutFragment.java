package com.dawson.ces.about;

import android.app.AlertDialog;
import android.os.Bundle;
import android.support.annotation.NonNull;
import android.support.annotation.Nullable;
import android.support.v4.app.Fragment;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.ImageView;
import com.dawson.ces.feces.R;

import java.util.ArrayList;
import java.util.Arrays;
import java.util.List;

public class AboutFragment extends Fragment {

    @Override
    public View onCreateView(@NonNull LayoutInflater inflater, ViewGroup container, Bundle savedInstanceState) {
        return inflater.inflate(R.layout.about, container, false);
    }

    @Override
    public void onViewCreated(@NonNull View view, @Nullable Bundle savedInstanceState) {
        super.onViewCreated(view, savedInstanceState);
        ImageView img1 = view.findViewById(R.id.sammyPic);
        ImageView img2 = view.findViewById(R.id.yanikPic);
        ImageView img3 = view.findViewById(R.id.diegoPic);
        ImageView img4 = view.findViewById(R.id.huanPic);
        List<ImageView> imgList = new ArrayList<>(Arrays.asList(img1, img2, img3, img4));
        setClickEvent(imgList);
    }

    private void setClickEvent(List<ImageView> imgList) {
        for (ImageView img : imgList) {
            img.setOnClickListener(this::dialogShow);
        }
    }

    private void dialogShow(View view) {
        AlertDialog.Builder builder = new AlertDialog.Builder(getActivity());
        switch (view.getId()) {
            case R.id.sammyPic:
                builder.setMessage(R.string.about_sammy);
                break;
            case R.id.huanPic:
                builder.setMessage(R.string.about_huan);
                break;
            case R.id.yanikPic:
                builder.setMessage(R.string.about_yanik);
                break;
            case R.id.diegoPic:
                builder.setMessage(R.string.about_diego);
                break;
        }
        builder.show();
    }
}
