package com.dawson.ces.loancalculator.fragments;

import android.Manifest;
import android.content.ContentResolver;
import android.content.Intent;
import android.content.pm.PackageManager;
import android.net.Uri;
import android.os.Bundle;
import android.support.v4.app.Fragment;
import android.database.Cursor;
import android.provider.ContactsContract;
import android.support.annotation.NonNull;
import android.support.annotation.Nullable;
import android.support.v4.content.ContextCompat;
import android.support.v7.widget.LinearLayoutManager;
import android.support.v7.widget.RecyclerView;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.Button;
import android.widget.EditText;


import com.dawson.ces.feces.R;
import com.dawson.ces.loancalculator.adapter.ContactDisplayInfoAdapter;
import com.dawson.ces.loancalculator.adapter.OnClickContactInfo;
import com.dawson.ces.loancalculator.business.ContactDisplayInfo;

import java.util.ArrayList;
import java.util.List;

public class ContactList extends Fragment implements OnClickContactInfo {
    //================================================================================
    // Properties
    //================================================================================
    private EditText search;
    private RecyclerView contacts;
    private List<ContactDisplayInfo> contactList;


    //================================================================================
    // Fragment Overridden Methods
    //================================================================================
    @Override
    public void onSaveInstanceState(@NonNull Bundle outState) {
        super.onSaveInstanceState(outState);
        outState.putString("searchInput", search.getText().toString());
        outState.putParcelableArrayList("contacts", new ArrayList<ContactDisplayInfo>(contactList));
    }


    @Override
    public View onCreateView(@NonNull LayoutInflater inflater, @Nullable ViewGroup container,
                             @Nullable Bundle savedInstanceState) {
        return inflater.inflate(R.layout.fragment_contact_list, container, false);
    }


    @Override
    public void onViewCreated(@NonNull View view, @Nullable Bundle savedInstanceState) {
        super.onViewCreated(view, savedInstanceState);
        search = view.findViewById(R.id.contactName);
        contacts = view.findViewById(R.id.contactList);
        contactList = new ArrayList<>();
        Button search = view.findViewById(R.id.searchButton);
        if (savedInstanceState != null) {
            this.search.setText(savedInstanceState.getString("searchInput"));
            contactList = savedInstanceState.getParcelableArrayList("contacts");
            final LinearLayoutManager layoutManager = new LinearLayoutManager(getContext());
            ContactDisplayInfoAdapter contactAdapter = new ContactDisplayInfoAdapter(this, contactList);
            contacts.setAdapter(contactAdapter);
            contacts.setLayoutManager(layoutManager);
        }
        search.setOnClickListener(v -> {
            contactList.clear();
            displayContacts();
        });
    }

    @Override
    public void onClickContactInfo(View v, int position) {
        sendEmail(contactList.get(position).getEmailAddress());
    }


    //================================================================================
    // Helper Methods
    //================================================================================

    /**
     * Gets all the Contacts that the user has in his contacts which match the user input.
     * It will depend populate the Recycler View.
     */
    private void displayContacts() {
        Cursor cursor = retrieveContact();
        while (cursor.moveToNext()) {
            ContactDisplayInfo contactDisplayInfo = new ContactDisplayInfo(cursor.getString(cursor.getColumnIndex(ContactsContract.Contacts.DISPLAY_NAME)), cursor.getString(cursor.getColumnIndex(ContactsContract.CommonDataKinds.Email.DATA)));
            contactList.add(contactDisplayInfo);
        }
        final LinearLayoutManager layoutManager = new LinearLayoutManager(getContext());
        ContactDisplayInfoAdapter contactAdapter = new ContactDisplayInfoAdapter(this, contactList);
        contacts.setAdapter(contactAdapter);
        contacts.setLayoutManager(layoutManager);
    }


    /**
     * Returns a Cursor object that contains all the contacts that match
     * what the user has entered.
     * If the user has not entered data for search, it will return all of his contacts
     * @return
     */
    private Cursor retrieveContact() {
        ContentResolver contentResolver = getView().getContext().getContentResolver();
        Uri uri = ContactsContract.CommonDataKinds.Email.CONTENT_URI;
        String[] filters = new String[]{
                ContactsContract.RawContacts._ID,
                ContactsContract.Contacts.DISPLAY_NAME,
                ContactsContract.CommonDataKinds.Email.DATA,
                ContactsContract.CommonDataKinds.Email.ADDRESS,
                ContactsContract.CommonDataKinds.Email.TYPE,
                ContactsContract.CommonDataKinds.Email.LABEL
        };
        if (search.getText().toString().isEmpty()) {
            //If the user did not search for anything -- display all of his contacts.
            return contentResolver.query(uri, filters, null, null, null);
        } else {

            String query = ContactsContract.Contacts.DISPLAY_NAME + " LIKE '" + search.getText().toString() + "%'";
            return contentResolver.query(uri, filters, query, null, null);
        }
    }

    /**
     * Method to send an Email to a given address
     * @param toAddress -- Address to which the user will send his Email
     */
    private void sendEmail(String toAddress) {
        Intent intent = new Intent(Intent.ACTION_SEND);
        intent.setData(Uri.parse("mailto:"));
        String[] to = {toAddress};
        intent.putExtra(Intent.EXTRA_EMAIL, to);
        intent.putExtra(Intent.EXTRA_SUBJECT, R.string.Subject);
        intent.putExtra(Intent.EXTRA_TEXT, getString(R.string.MessageText) + getArguments().getString("calculation"));
        intent.setType("message/rfc822");
        Intent chose = Intent.createChooser(intent, "Send Email");
        startActivity(chose);
        startActivity(chose);

    }

}
