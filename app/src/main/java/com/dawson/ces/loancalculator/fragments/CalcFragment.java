package com.dawson.ces.loancalculator.fragments;

import android.Manifest;
import android.content.DialogInterface;
import android.content.pm.PackageManager;
import android.os.Bundle;
import android.support.annotation.NonNull;
import android.support.annotation.Nullable;
import android.support.v4.app.ActivityCompat;
import android.support.v4.app.Fragment;
import android.support.v4.app.FragmentTransaction;
import android.support.v4.content.ContextCompat;
import android.support.v7.app.AlertDialog;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.Button;
import android.widget.EditText;
import android.widget.TextView;
import android.widget.Toast;

import com.dawson.ces.feces.R;
import com.dawson.ces.loancalculator.business.Calculator;

public class CalcFragment extends Fragment {
    //================================================================================
    // Properties
    //================================================================================
    EditText loanAmount;
    EditText years;
    EditText yearlyInterestRate;
    Button calcButton;
    TextView minPayment;
    TextView totalPayment;
    TextView totalInterest;
    TextView result;
    double loanAmountVal;
    double yearlyInterestRateVal;
    int numYears;
    boolean sendEmail = false;
    Calculator lc;

    //================================================================================
    // Fragment Overridden Methods
    //================================================================================
    @Override
    public View onCreateView(@NonNull LayoutInflater inflater, @Nullable ViewGroup container,
                             @Nullable Bundle savedInstanceState) {
        return inflater.inflate(R.layout.cc_calculator, container, false);
    }

    @Override
    public void onSaveInstanceState(@NonNull Bundle outState) {
        super.onSaveInstanceState(outState);
        outState.putString("loanAmount", loanAmount.getText().toString());
        outState.putString("years", years.getText().toString());
        outState.putString("yearlyInterestRate", yearlyInterestRate.getText().toString());
        outState.putString("minPayment", minPayment.getText().toString());
        outState.putString("totalPayment", totalPayment.getText().toString());
        outState.putString("totalInterest", totalInterest.getText().toString());
        outState.putBoolean("sendEmail", this.sendEmail);
    }

    @Override
    public void onViewCreated(@NonNull View view, @Nullable Bundle savedInstanceState) {
        super.onViewCreated(view, savedInstanceState);

        loanAmount = view.findViewById(R.id.loanAmount);
        years = view.findViewById(R.id.years);
        yearlyInterestRate = view.findViewById(R.id.yearlyInterestRate);

        calcButton = view.findViewById(R.id.calcButton);
        calcButton.setOnClickListener(v -> doTheCalc(v));
        Button sendEmail = view.findViewById(R.id.sendEmail);
        sendEmail.setOnClickListener(v -> {
            if (this.sendEmail == true) {
                if(ContextCompat.checkSelfPermission(getContext(),Manifest.permission.READ_CONTACTS) != PackageManager.PERMISSION_GRANTED){
                    requestContactPermission();
                }
                else{
                    FragmentTransaction transaction = getFragmentManager().beginTransaction();
                    ContactList contactList = new ContactList();
                    Bundle b = new Bundle();
                    b.putString("calculation", lc.toString());
                    contactList.setArguments(b);
                    transaction.replace(R.id.content_frame, contactList);
                    transaction.addToBackStack(null);
                    transaction.commit();
                }

            }
        });
        minPayment = view.findViewById(R.id.minPayment);
        totalPayment = view.findViewById(R.id.totalPayment);
        totalInterest = view.findViewById(R.id.totalInterest);
        result = view.findViewById(R.id.result);
        if (savedInstanceState != null) {
            loanAmount.setText(savedInstanceState.getString("loanAmount"));
            years.setText(savedInstanceState.getString("years"));
            yearlyInterestRate.setText(savedInstanceState.getString("yearlyInterestRate"));
            minPayment.setText(savedInstanceState.getString("minPayment"));
            totalPayment.setText(savedInstanceState.getString("totalPayment"));
            totalInterest.setText(savedInstanceState.getString("totalInterest"));
            this.sendEmail = savedInstanceState.getBoolean("sendEmail");
            data();
            lc = new Calculator(loanAmountVal, numYears, yearlyInterestRateVal);
        }

    }

    private void requestContactPermission() {
        if(ActivityCompat.shouldShowRequestPermissionRationale(getActivity(),Manifest.permission.READ_CONTACTS)){
                new AlertDialog.Builder(getContext())
                        .setTitle(R.string.PermissionNeeded)
                        .setMessage(R.string.DialogMessage)
                        .setPositiveButton(R.string.Ok, new DialogInterface.OnClickListener() {
                            @Override
                            public void onClick(DialogInterface dialog, int which) {
                                ActivityCompat.requestPermissions(getActivity(),new String[]{Manifest.permission.READ_CONTACTS},1);
                            }
                        })
                        .setNegativeButton(R.string.Cancel, new DialogInterface.OnClickListener() {
                            @Override
                            public void onClick(DialogInterface dialog, int which) {
                                dialog.dismiss();
                            }
                        }).show();
        }else{
            ActivityCompat.requestPermissions(getActivity(),new String[]{Manifest.permission.READ_CONTACTS},1);
        }
    }

    @Override
    public void onRequestPermissionsResult(int requestCode, @NonNull String[] permissions, @NonNull int[] grantResults) {
        if(requestCode == 1){
            if(grantResults.length>0 && grantResults[0] == PackageManager.PERMISSION_GRANTED){
                Toast.makeText(getContext(),getString(R.string.PermissionGranted),Toast.LENGTH_LONG).show();
            }
        }
    }

    //================================================================================
    // Helper Methods
    //================================================================================
    private void doTheCalc(View v) {
        if (validInput()) {
            lc = new Calculator(loanAmountVal, numYears, yearlyInterestRateVal);
            minPayment.setText(Double.toString(lc.getMonthlyPayment()));
            totalPayment.setText(Double.toString(lc.getTotalCostOfLoan()));
            totalInterest.setText(Double.toString(lc.getTotalInterest()));
            sendEmail = true;
        } else {
            result.setText(R.string.not_a_valid_number);
            sendEmail = false;
        }
    }

    //Validate User input
    private boolean validInput() {
        try {
            data();
            if (numYears == 0 || yearlyInterestRateVal == 0 || loanAmountVal == 0)
                return false;

            else
                return true;
        } catch (NumberFormatException nfe) {
            return false;
        }


    }

    private void data() {
        try {
            loanAmountVal = Double.parseDouble(loanAmount.getText().toString());
            yearlyInterestRateVal = Double.parseDouble(yearlyInterestRate.getText().toString());
            numYears = Integer.parseInt(years.getText().toString());
        } catch (NumberFormatException nfe) {
        }

    }

}
