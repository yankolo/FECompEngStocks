package com.dawson.ces.loancalculator.adapter;

import android.support.annotation.NonNull;
import android.support.v7.widget.RecyclerView;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.TextView;
import com.dawson.ces.feces.R;
import com.dawson.ces.loancalculator.business.ContactDisplayInfo;

import java.util.List;

public class ContactDisplayInfoAdapter extends RecyclerView.Adapter<ContactDisplayInfoAdapter.MyViewHolder>
{
    //================================================================================
    // Properties
    //================================================================================
    private List<ContactDisplayInfo> contactInfo;
    private OnClickContactInfo display;

    //================================================================================
    // Constructors
    //================================================================================
    public ContactDisplayInfoAdapter(OnClickContactInfo display, List<ContactDisplayInfo> contactInfo) {
        this.display = display;
        this.contactInfo = contactInfo;
    }
    //================================================================================
    // Recycler.Adapter Overridden Methods
    //================================================================================
    @NonNull
    @Override
    public MyViewHolder onCreateViewHolder(@NonNull ViewGroup viewGroup, int i) {
        View v = LayoutInflater.from(viewGroup.getContext())
                .inflate(R.layout.fragment_contact_display, viewGroup, false);
        MyViewHolder vh = new MyViewHolder(v);
        return vh;
    }
    @Override
    public void onBindViewHolder(@NonNull MyViewHolder viewHolder, int i) {
        viewHolder.userInfo.setText(contactInfo.get(i).toString());
    }

    @Override
    public int getItemCount() {
        return contactInfo.size();
    }
    public class MyViewHolder extends RecyclerView.ViewHolder implements View.OnClickListener {
         TextView userInfo;

        public MyViewHolder(View v) {
            super(v);
            userInfo = itemView.findViewById(R.id.userinfo);
            v.setOnClickListener(this);
        }

        @Override
        public void onClick(View v) {
            display.onClickContactInfo(v,getLayoutPosition());
        }
    }
}
