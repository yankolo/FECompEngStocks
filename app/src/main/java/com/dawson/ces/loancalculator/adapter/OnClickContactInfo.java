package com.dawson.ces.loancalculator.adapter;

import android.view.View;
//Interface to communicate between Adapter and Fragment
public interface OnClickContactInfo {

     void onClickContactInfo(View v, int position);

}
