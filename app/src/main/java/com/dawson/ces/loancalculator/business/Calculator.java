package com.dawson.ces.loancalculator.business;

public class Calculator {
    //================================================================================
    // Properties
    //================================================================================
    private double loanAmount;
    private int numberOfYears;
    private double yearlyInterestRate;

    //================================================================================
    // Constructors
    //================================================================================
    public Calculator(double loanAmount, int numberOfYears,
                      double yearlyInterestRate) {
        this.loanAmount = loanAmount;
        this.numberOfYears = numberOfYears;
        this.yearlyInterestRate = yearlyInterestRate;
    }

    //================================================================================
    // Instance Methods
    //================================================================================
    public double getLoanAmount() {
        return loanAmount;
    }


    public double getMonthlyPayment() {
        double monthlyPayment;
        double monthlyInterestRate;
        int numberOfPayments;
        if (numberOfYears != 0 && yearlyInterestRate != 0) {
            //calculate the monthly payment
            monthlyInterestRate = yearlyInterestRate / 1200;
            numberOfPayments = numberOfYears * 12;

            monthlyPayment =
                    (loanAmount * monthlyInterestRate) /
                            (1 - (1 / Math.pow((1 + monthlyInterestRate), numberOfPayments)));

            monthlyPayment = Math.round(monthlyPayment * 100) / 100.0;
        } else
            monthlyPayment = 0;
        return monthlyPayment;
    }


    public int getNumberOfYears() {
        return numberOfYears;
    }


    public double getTotalCostOfLoan() {
        return getMonthlyPayment() * numberOfYears * 12;

    }


    public double getTotalInterest() {
        return getTotalCostOfLoan() - loanAmount;
    }


    public double getYearlyInterestRate() {
        return yearlyInterestRate;
    }


    public String toString() {
        return "\nThis is the Loan Amount: " + getLoanAmount() + "\n" + "This is the number of years it will take to pay off that loan: " + getNumberOfYears() + "\n" +
                "This is the yearly interest rate: " + getYearlyInterestRate() + "\n"
                + "This is the monthly payment: " + getMonthlyPayment() + "\n"
                +"This is the total Cost of the Loan: " + getTotalCostOfLoan() + "\n"
                + "This is the total interest rate: " + getTotalInterest();
    }
}
