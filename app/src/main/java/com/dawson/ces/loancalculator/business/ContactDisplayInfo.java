package com.dawson.ces.loancalculator.business;

import android.os.Parcel;
import android.os.Parcelable;

public class ContactDisplayInfo implements Parcelable {
    //================================================================================
    // Properties
    //================================================================================
    private String username;
    private String emailAddress;

    //================================================================================
    // Constructors
    //================================================================================

    public ContactDisplayInfo(String username, String emailAddress) {
        this.username = username;
        this.emailAddress = emailAddress;
    }

    //================================================================================
    // Fragment Overridden Methods
    //================================================================================
    protected ContactDisplayInfo(Parcel in) {
        username = in.readString();
        emailAddress = in.readString();
    }

    @Override
    public void writeToParcel(Parcel dest, int flags) {
        dest.writeString(username);
        dest.writeString(emailAddress);
    }

    @Override
    public int describeContents() {
        return 0;
    }

    public static final Creator<ContactDisplayInfo> CREATOR = new Creator<ContactDisplayInfo>() {
        @Override
        public ContactDisplayInfo createFromParcel(Parcel in) {
            return new ContactDisplayInfo(in);
        }

        @Override
        public ContactDisplayInfo[] newArray(int size) {
            return new ContactDisplayInfo[size];
        }
    };

    //================================================================================
    // Get Methods
    //================================================================================
    public String getUsername() {
        return username;
    }

    public String getEmailAddress() {
        return emailAddress;
    }

    @Override
    public String toString() {
        return username + "\n" + emailAddress;
    }
}
