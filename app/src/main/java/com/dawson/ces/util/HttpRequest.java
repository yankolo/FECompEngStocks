package com.dawson.ces.util;

import java.io.BufferedReader;
import java.io.BufferedWriter;
import java.io.IOException;
import java.io.InputStream;
import java.io.InputStreamReader;
import java.io.OutputStream;
import java.io.OutputStreamWriter;
import java.net.HttpURLConnection;
import java.net.MalformedURLException;
import java.net.URL;
import java.util.ArrayList;
import java.util.List;

public class HttpRequest {
    private String url;
    private String method;
    private List<String> headerKeys = new ArrayList<>();
    private List<String> headerValues = new ArrayList<>();
    private String body;

    public void setUrl(String url) {
        this.url = url;
    }

    public void setMethod(String method) {
        this.method = method;
    }

    public void appendHeader(String key, String value) {
        headerKeys.add(key);
        headerValues.add(value);
    }

    public void setBody(String body) {
        this.body = body;
    }

    public HttpResponse execute() {
        try {
            URL url = new URL(this.url);
            HttpURLConnection http = (HttpURLConnection) url.openConnection();
            http.setRequestMethod(this.method);
            for (int i = 0; i < headerValues.size(); i++) {
                http.setRequestProperty(headerKeys.get(i), headerValues.get(i));
            }

            // If the HTTP method is POST, write the body to the request
            if (this.method.equals("POST")) {
                OutputStream out = http.getOutputStream();
                BufferedWriter bufferedWriter = new BufferedWriter(new OutputStreamWriter(out));
                bufferedWriter.write(this.body);
                bufferedWriter.flush();
                bufferedWriter.close();
            }

            return new HttpResponse(http);
        } catch (MalformedURLException e) {
            throw new RuntimeException(e);
        } catch (IOException e) {
            throw new RuntimeException(e);
        }
    }
}
