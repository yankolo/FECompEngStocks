package com.dawson.ces.util;

import java.io.BufferedReader;
import java.io.IOException;
import java.io.InputStream;
import java.io.InputStreamReader;
import java.net.HttpURLConnection;

public class HttpResponse {
    private HttpURLConnection http;

    /**
     * Package private constructor so that only the HttpRequest can instantiate an HttpResponse
     */
    HttpResponse(HttpURLConnection http) {
        this.http = http;
    }

    public String getBody() {
        StringBuilder response = new StringBuilder();
        String currentLine = "";

        try {
            InputStream in = http.getResponseCode() <= 399 ? http.getInputStream() : http.getErrorStream();
            BufferedReader bufferedReader = new BufferedReader(new InputStreamReader(in));
            while (currentLine != null) {
                currentLine = bufferedReader.readLine();
                response.append(currentLine);
            }
        } catch (IOException e) {
            throw new RuntimeException(e);
        }

        return response.toString();
    }

    public int getResponseCode() {
        int responseCode = 0;

        try {
            responseCode = http.getResponseCode();
        } catch (IOException e) {
            throw new RuntimeException(e);
        }

        return responseCode;
    }
}
