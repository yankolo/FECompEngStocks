package com.dawson.ces.main.fragments;

import android.os.Bundle;
import android.support.annotation.NonNull;
import android.support.annotation.Nullable;
import android.support.v4.app.Fragment;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;

import com.dawson.ces.feces.R;
import com.dawson.ces.main.activities.DrawerActivity;

public class MainFragment extends Fragment {

    private final static int[] DASHBOARD_ITEMS = new int[]{
            R.id.nav_foreign_exchange,
            R.id.nav_financial_hints,
            R.id.nav_notes,
            R.id.nav_credit_loan_calculator,
            R.id.nav_stock_portfolio
    };

    @Override
    public View onCreateView(@NonNull LayoutInflater inflater, ViewGroup container,
                             Bundle savedInstanceState) {
        return inflater.inflate(R.layout.activity_main, container, false);
    }

    @Override
    public void onActivityCreated(@Nullable Bundle savedInstanceState) {
        super.onActivityCreated(savedInstanceState);

        final DrawerActivity dr = (DrawerActivity) this.getActivity();

        if (dr != null) {
            for (int dashboardItem : DASHBOARD_ITEMS) {
                this
                        .getView()
                        .findViewById(dashboardItem)
                        .setOnClickListener(clickView
                                -> dr.handleNavigationSelection(dashboardItem)
                        );
            }
        }
    }
}
