package com.dawson.ces.main.activities;

import android.content.Intent;
import android.content.SharedPreferences;
import android.net.Uri;
import android.os.Bundle;
import android.support.annotation.NonNull;
import android.support.annotation.Nullable;
import android.support.design.widget.NavigationView;
import android.support.v4.app.Fragment;
import android.support.v4.app.FragmentManager;
import android.support.v4.app.FragmentTransaction;
import android.support.v4.view.GravityCompat;
import android.support.v4.widget.DrawerLayout;
import android.support.v7.app.ActionBarDrawerToggle;
import android.support.v7.app.AppCompatActivity;
import android.support.v7.widget.Toolbar;
import android.util.Log;
import android.view.MenuItem;
import android.widget.TextView;
import com.dawson.ces.about.AboutFragment;
import com.dawson.ces.feces.R;
import com.dawson.ces.financialhints.fragments.HintFragment;
import com.dawson.ces.forex.fragments.ForexFragment;
import com.dawson.ces.loancalculator.fragments.CalcFragment;
import com.dawson.ces.main.fragments.MainFragment;
import com.dawson.ces.notes.fragments.NoteFragment;
import com.dawson.ces.settings.activities.SettingsActivity;
import com.dawson.ces.stocks.fragments.LoginFragment;

/**
 * The main class of the app, which handles the drawer and creating fragments which compose the rest
 * of the app
 *
 * @author Diego Plaza-Ottereyes
 */
public class DrawerActivity extends AppCompatActivity
        implements NavigationView.OnNavigationItemSelectedListener {

    private static final String CURRENT_FRAGMENT_KEY = "CURRENT_FRAGMENT_ID";
    private static final String TAG = "DRAWER-ACT";

    /**
     * The fragment currently being displayed
     */
    private int currentFragmentId = -1;
    private DrawerLayout drawer;
    private NavigationView navigationView;

    @Override
    protected void onCreate(@Nullable Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);

        this.setContentView(R.layout.activity_drawer);
        Toolbar toolbar = findViewById(R.id.toolbar);
        setSupportActionBar(toolbar);

        this.drawer = findViewById(R.id.drawer_layout);
        ActionBarDrawerToggle toggle = new ActionBarDrawerToggle(
                this, drawer, toolbar, R.string.navigation_drawer_open, R.string.navigation_drawer_close);
        drawer.addDrawerListener(toggle);
        toggle.syncState();

        this.navigationView = findViewById(R.id.nav_view);
        navigationView.setNavigationItemSelectedListener(this);

        // If the user wasn't looking at a fragment, set the current fragment to the dashboard view
        if (savedInstanceState != null) {
            currentFragmentId = savedInstanceState.getInt(CURRENT_FRAGMENT_KEY, -1);
        }

        if (currentFragmentId == -1) {
            this.handleNavigationSelection(R.id.nav_dashboard);
        } else {
            // Restore the title, the actual fragment isn't important since android does it for us automatically
            // and trying to do it ourselves results in the fragment being created twice, which breaks trying to save
            // things in the bundle (eg. when rotating)
            this.handleNavigationSelection(currentFragmentId, true);
        }
    }

    @Override
    protected void onStart() {
        super.onStart();

        SharedPreferences prefs =
                getSharedPreferences(getString(R.string.prefs_settings), MODE_PRIVATE);

        TextView navTitle =
                navigationView.getHeaderView(0).findViewById(R.id.nav_header_title);
        TextView navSubtitle =
                navigationView.getHeaderView(0).findViewById(R.id.nav_header_subtitle);

        navTitle.setText(
                prefs.getString(getString(R.string.prefs_first_name), getString(R.string.not_logged_in)));
        navSubtitle.setText(prefs.getString(getString(R.string.prefs_email), "-"));
    }

    @Override
    public void onSaveInstanceState(Bundle savedInstanceState) {
        savedInstanceState.putInt(CURRENT_FRAGMENT_KEY, currentFragmentId);

        super.onSaveInstanceState(savedInstanceState);
    }

    @Override
    public void onBackPressed() {
        SharedPreferences prefs = getSharedPreferences(getString(R.string.prefs_settings),
                MODE_PRIVATE);

        boolean backOpensDrawer = prefs.getBoolean(getString(R.string.prefs_back_opens_drawer), false);

        if (this.drawer.isDrawerOpen(GravityCompat.START)) {
            this.drawer.closeDrawer(GravityCompat.START);
        } else {
            if (getSupportFragmentManager().getBackStackEntryCount() > 0) {
                getSupportFragmentManager().popBackStack();
            } else if (backOpensDrawer) {
                this.drawer.openDrawer(GravityCompat.START);
            } else if (currentFragmentId != R.id.nav_dashboard) {
                // First return to dashboard before exiting
                handleNavigationSelection(R.id.nav_dashboard);
            } else {
                super.onBackPressed();
            }
        }
    }

    @Override
    public boolean onNavigationItemSelected(@NonNull MenuItem item) {
        // Handle navigation view item clicks here.
        handleNavigationSelection(item.getItemId());

        drawer.closeDrawer(GravityCompat.START);
        return true;
    }

    /**
     * Handle a navigation item selected by creating and showing a fragment, or firing up an intent
     *
     * @param id The id of the navigation item selected
     */
    public void handleNavigationSelection(int id) {
        this.handleNavigationSelection(id, false);
    }

    /**
     * Handle a navigation item selected by creating and showing a fragment, or firing up an intent
     *
     * @param id           The id of the navigation item selected
     * @param onlySetTitle If no fragment or intent should be fired, and rather just set the title
     */
    public void handleNavigationSelection(int id, boolean onlySetTitle) {
        Log.d(TAG, "Handling a navigation selection of: " + id);

        // Clicking on dawson opens an intent, not a fragment
        if (id != R.id.nav_dawson && id != R.id.nav_settings) {
            currentFragmentId = id;
        }

        switch (id) {
            case R.id.nav_dashboard:
                this.setTitleAndFragment(R.string.dashboard, onlySetTitle ? null : new MainFragment());
                break;
            case R.id.nav_foreign_exchange:
                this.setTitleAndFragment(R.string.foreign_exchange, onlySetTitle ? null : new ForexFragment());
                break;
            case R.id.nav_financial_hints:
                this.setTitleAndFragment(R.string.financial_hints, onlySetTitle ? null : new HintFragment());
                break;
            case R.id.nav_notes:
                this.setTitleAndFragment(R.string.notes, onlySetTitle ? null : new NoteFragment());
                break;
            case R.id.nav_credit_loan_calculator:
                this.setTitleAndFragment(R.string.credit_loan_calculator, onlySetTitle ? null : new CalcFragment());
                break;
            case R.id.nav_stock_portfolio:
                this.setTitle(R.string.stock_portfolio_title_in_appbar);
                LoginFragment loginFragment = new LoginFragment();
                this.setViewedFragment(loginFragment);
                break;
            case R.id.nav_settings:
                Intent iSettings = new Intent(this, SettingsActivity.class);
                startActivity(iSettings);
                break;
            case R.id.nav_about:
                this.setTitleAndFragment(R.string.about, onlySetTitle ? null : new AboutFragment());
                break;
            case R.id.nav_dawson:
                Intent iDawson = new Intent(
                        Intent.ACTION_VIEW,
                        Uri.parse("https://www.dawsoncollege.qc.ca/")
                );

                if (this.getPackageManager().resolveActivity(iDawson, iDawson.getFlags()) != null) {
                    startActivity(iDawson);
                }
                break;
        }

        navigationView.setCheckedItem(id);
        drawer.closeDrawer(GravityCompat.START);
    }

    /**
     * Sets the current title and fragment, if there's a non-null fragment
     *
     * @param title The string resource id of the title to set
     * @param fragment The fragment to set, if non-null
     */
    private void setTitleAndFragment(int title, @Nullable Fragment fragment) {
        this.setTitle(title);
        if (fragment != null) {
            this.setViewedFragment(fragment);
        }
    }

    /**
     * Sets the currently viewed fragment, replacing the old fragment
     *
     * @param fragment The fragment to replace the old one with
     */
    private void setViewedFragment(Fragment fragment) {
        FragmentManager fragmentManager = this.getSupportFragmentManager();
        FragmentTransaction fragmentTransaction = fragmentManager.beginTransaction();

        fragmentTransaction.replace(R.id.content_frame, fragment);
        fragmentTransaction.setTransition(FragmentTransaction.TRANSIT_FRAGMENT_FADE);

        fragmentTransaction.commit();
    }
}