package com.dawson.ces.financialhints.persistence;

import com.dawson.ces.financialhints.data.Hint;
import com.google.firebase.database.DataSnapshot;
import com.google.firebase.database.DatabaseReference;
import com.google.firebase.database.FirebaseDatabase;

import java.util.ArrayList;
import java.util.List;
import java.util.Map;

/**
 * The FirebaseManager is general class that makes the interaction with the Firebase API easier.
 * The class might be used throughout the whole application, which makes
 * it a good candidate for the Singleton pattern.
 * <p>
 * A Singleton is used (instead of static methods) as in the future the
 * FirebaseManager might contain some metadata about the Firebase connection
 * (e.g. logged in user information) and having a single object containing this
 * information makes it easier to manage it.
 * <p>
 * To get the only instance of the FirebaseManager use the getInstance() method.
 *
 * @author Yanik
 */
public class FirebaseManager {
    // FirebaseManager is a Singleton (only one instance can exist)
    private static FirebaseManager firebaseManager;

    /**
     * Private constructor makes it so that the only way to instantiate the object
     * is using the getInstance() method
     */
    private FirebaseManager() {
        // Prevent access to constructor using Reflection
        if (firebaseManager != null) {
            throw new RuntimeException("Use getInstance() method to get the single instance of this class.");
        }
    }

    /**
     * The method that is used to get the single instance of a FirebaseManager.
     * Lazily instantiates the object.
     *
     * @return The single instance of a FirebaseManager
     */
    public synchronized static FirebaseManager getInstance() {
        if (firebaseManager == null) {
            firebaseManager = new FirebaseManager();
        }

        return firebaseManager;
    }

    /**
     * Gets a Firebase database reference that points to a node that
     * contains a list of hints.
     *
     * @return Firebase database reference that points to a node that
     * contains a list of hints.
     * @author Diego Plaza-Ottereyes
     */
    public DatabaseReference getHintListReference() {
        return FirebaseDatabase.getInstance().getReference().child("hints");
    }

    /**
     * Creates a Hint List from a DataSnapshot
     *
     * @param hintListDataSnapshot A DataSnapshot that contains a list of hints
     * @return A list of hints that was constructed from the DataSnapshot
     * @author Diego Plaza-Ottereyes
     */
    public List<Hint> buildHintList(DataSnapshot hintListDataSnapshot) {
        List<Hint> hintList = new ArrayList<>();

        for (DataSnapshot hintDataSnapshot : hintListDataSnapshot.getChildren()) {
            Hint h = new Hint();
            Map<String, String> json = (Map<String, String>) hintDataSnapshot.getValue();
            h.setText(json.get("text"));
            h.setSrc(json.get("src"));

            hintList.add(h);
        }

        return hintList;
    }

}
