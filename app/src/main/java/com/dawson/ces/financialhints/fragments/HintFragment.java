package com.dawson.ces.financialhints.fragments;

import android.os.Bundle;
import android.support.annotation.NonNull;
import android.support.annotation.Nullable;
import android.support.v4.app.Fragment;
import android.support.v4.view.ViewPager;
import android.util.Log;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;

import com.dawson.ces.feces.R;
import com.dawson.ces.financialhints.adapters.HintPagerAdapter;
import com.dawson.ces.financialhints.data.Hint;
import com.dawson.ces.financialhints.persistence.FirebaseManager;
import com.google.firebase.database.DataSnapshot;
import com.google.firebase.database.DatabaseError;
import com.google.firebase.database.DatabaseReference;
import com.google.firebase.database.ValueEventListener;

import java.util.ArrayList;
import java.util.Collections;
import java.util.List;

public class HintFragment extends Fragment {

    private final static String TAG = "HINT-FRAGMENT";

    private List<Hint> hints = new ArrayList<>();
    private ViewPager hintPager;
    private HintPagerAdapter hintPagerAdapter;

    @Override
    public View onCreateView(@NonNull LayoutInflater inflater, @Nullable ViewGroup container,
                             @Nullable Bundle savedInstanceState) {
        return inflater.inflate(R.layout.hints_pager, container, false);
    }

    @Override
    public void onViewCreated(@NonNull View view, @Nullable Bundle savedInstanceState) {
        super.onViewCreated(view, savedInstanceState);

        Log.d(TAG, "On create reached");

        hintPager = view.findViewById(R.id.hint_viewpager);
        loadData(savedInstanceState);
    }

    @Override
    public void onSaveInstanceState(@NonNull Bundle outState) {
        super.onSaveInstanceState(outState);
        outState.putParcelableArrayList("hints", new ArrayList<>(hints));
    }

    private void setupPager() {
        hintPagerAdapter = new HintPagerAdapter(getFragmentManager(), hints);
        hintPager.setAdapter(hintPagerAdapter);

        // Make the adjacent pages partially visible
        hintPager.setClipToPadding(false);
        hintPager.setPageMargin(
                getResources().getDimensionPixelOffset(R.dimen.hint_pager_space_between_pages_half));
        Log.d(TAG, "Hint pager setup");
    }

    /**
     * Helper method to load the list of cards from the Firebase database *or* the bundle, if cached
     */
    private void loadData(@Nullable Bundle savedSate) {
        // If we have some cached bundle data, meaning the user rotated the screen so let's keep the current order
        if (savedSate != null && savedSate.containsKey("hints")) {
            Log.d(TAG, "Loading hints from bundle");
            hints = savedSate.getParcelableArrayList("hints");

            if (hints != null && hints.size() > 0) {
                setupPager();
                return;
            }
            Log.d(TAG, "Bundled hints were empty, trying from firebase");
        }

        FirebaseManager firebaseManager = FirebaseManager.getInstance();
        DatabaseReference databaseReference = firebaseManager.getHintListReference();

        databaseReference.addListenerForSingleValueEvent(new ValueEventListener() {
            @Override
            public void onDataChange(DataSnapshot dataSnapshot) {
                Log.d(TAG, "Data changed called");

                if (getContext() == null) {
                    // I like how this is a real issue, you can't rotate the screen while firebase reads from the db lol
                    Log.w(TAG, "Got data change, but was not attached to a context. " +
                            "Was the display rotated before loading?");
                    return;
                }

                List<Hint> dbHints = firebaseManager.buildHintList(dataSnapshot);
                // The specs say we need to pick a random element and then display it
                // However I decided to instead give the user a nice card UI and just shuffle the cards
                // before displaying them to the user :)
                Collections.shuffle(dbHints);

                hints.addAll(dbHints);
                setupPager();
            }

            @Override
            public void onCancelled(DatabaseError databaseError) {
                Log.w(TAG, "Value event listener cancelled", databaseError.toException());
            }
        });

        Log.d(TAG, "Data listener added");
    }
}
