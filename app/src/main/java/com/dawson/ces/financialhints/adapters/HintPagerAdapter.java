package com.dawson.ces.financialhints.adapters;

import android.os.Bundle;
import android.support.annotation.NonNull;
import android.support.v4.app.Fragment;
import android.support.v4.app.FragmentManager;
import android.support.v4.app.FragmentStatePagerAdapter;
import android.util.Log;
import android.util.SparseArray;
import android.view.ViewGroup;

import com.dawson.ces.financialhints.data.Hint;
import com.dawson.ces.financialhints.fragments.HintCardFragment;

import java.util.List;

public class HintPagerAdapter extends FragmentStatePagerAdapter {

    private final static String TAG = "HINT-PAGER-ADAPTER";

    // Use a sparse array in case we don't get the request to instantiate cards in order
    // (A sparse array will allow to access arbitrary indexes that aren't already
    // defined unlike ArrayList)
    private SparseArray<Fragment> hintFragments = new SparseArray<>();
    private List<Hint> hints;

    public HintPagerAdapter(FragmentManager fm, List<Hint> hints) {
        super(fm);
        this.hints = hints;
    }

    @NonNull
    @Override
    public Object instantiateItem(@NonNull ViewGroup container, int position) {
        Log.d(TAG, "Instantiating new item @ " + position);
        Fragment fragment = new HintCardFragment();
        Hint hint = hints.get(position);

        Bundle b = new Bundle();
        b.putString("TEXT", hint.getText());
        b.putString("SRC", hint.getSrc());
        fragment.setArguments(b);

        hintFragments.put(position, fragment);

        return super.instantiateItem(container, position);
    }

    @Override
    public void destroyItem(@NonNull ViewGroup container, int position, @NonNull Object object) {
        Log.d(TAG, "Destroying item @ " + position);
        hintFragments.remove(position);

        super.destroyItem(container, position, object);
    }

    @Override
    public Fragment getItem(int i) {
        Log.d(TAG, "Getting item @ " + i);
        return hintFragments.get(i);
    }

    @Override
    public int getCount() {
        return hints.size();
    }
}
