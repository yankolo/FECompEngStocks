package com.dawson.ces.financialhints.data;

import android.os.Parcel;
import android.os.Parcelable;

public class Hint implements Parcelable {

    private String text;
    private String src;

    public Hint() {
    }

    public Hint(String text, String src) {
        this.text = text;
        this.src = src;
    }

    /**
     * @return text
     */
    public String getText() {
        return text;
    }

    /**
     * @param text New value for text
     */
    public void setText(String text) {
        this.text = text;
    }

    /**
     * @return src
     */
    public String getSrc() {
        return src;
    }

    /**
     * @param src New value for src
     */
    public void setSrc(String src) {
        this.src = src;
    }

  @Override
  public int describeContents() {
    return 0;
  }

  @Override
  public void writeToParcel(Parcel dest, int flags) {
    dest.writeString(text);
    dest.writeString(src);
  }

  public static final Parcelable.Creator<Hint> CREATOR = new Creator<Hint>() {
    @Override
    public Hint createFromParcel(Parcel source) {
      return new Hint(source.readString(), source.readString());
    }

    @Override
    public Hint[] newArray(int size) {
      return new Hint[size];
    }
  };
}
