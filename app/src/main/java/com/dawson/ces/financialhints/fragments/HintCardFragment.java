package com.dawson.ces.financialhints.fragments;

import android.content.Intent;
import android.net.Uri;
import android.os.Bundle;
import android.support.annotation.NonNull;
import android.support.annotation.Nullable;
import android.support.v4.app.Fragment;
import android.util.Log;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.Button;
import android.widget.TextView;

import com.dawson.ces.feces.R;

public class HintCardFragment extends Fragment {

    private final static String TAG = "HINT-CARD-FRAGMENT";

    private String text = "Empty";
    private String src = "https://google.com";

    @Override
    public void onCreate(@Nullable Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);

        src = this.getArguments().getString("SRC");
        text = this.getArguments().getString("TEXT");

        Log.d(TAG, "Creating hint card > " + text);
    }

    @Nullable
    @Override
    public View onCreateView(@NonNull LayoutInflater inflater, @Nullable ViewGroup container,
                             @Nullable Bundle savedInstanceState) {
        Log.d(TAG, "Creating hint card view");
        return inflater.inflate(R.layout.hint_card, container, false);
    }

    @Override
    public void onViewCreated(@NonNull View view, @Nullable Bundle savedInstanceState) {
        super.onViewCreated(view, savedInstanceState);

        Log.d(TAG, "Created hint card view, setting up view");

        TextView tv = view.findViewById(R.id.hint_text);
        tv.setText(this.text);

        Button button = view.findViewById(R.id.hint_source);

        // Setup the "source" button to open up the source url in the user's browser
        // (Or whatever they have setup to view http uris, maybe they use curl on android)
        button.setOnClickListener(v -> {
            Intent i = new Intent(Intent.ACTION_VIEW);
            i.setData(Uri.parse(this.src));
            startActivity(i);
        });
    }
}
