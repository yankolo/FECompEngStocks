package com.dawson.ces.settings.enums;

import android.support.annotation.Nullable;

/**
 * Represents a Stock Exchange
 */
public enum Stock {
    TSX(0), NYSE(1), NASDAQ(2);

    private int id;

    Stock(int id) {
        this.id = id;
    }

    /**
     * @return id
     */
    public int getId() {
        return id;
    }

    /**
     * Get the matching stock for an id
     *
     * @param id The id of the stock you want to lookup
     * @return The stock, if found, else null
     */
    public static @Nullable
    Stock fromId(int id) {
        for (Stock stock : Stock.values()) {
            if (stock.id == id) {
                return stock;
            }
        }

        return null;
    }
}
