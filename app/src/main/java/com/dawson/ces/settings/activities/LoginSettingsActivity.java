package com.dawson.ces.settings.activities;

import android.content.SharedPreferences;
import android.os.Bundle;
import android.support.design.widget.FloatingActionButton;
import android.support.design.widget.Snackbar;
import android.support.v7.app.AlertDialog;
import android.support.v7.app.AppCompatActivity;
import android.support.v7.widget.Toolbar;
import android.text.Editable;
import android.text.TextWatcher;
import android.util.Patterns;
import android.widget.EditText;
import android.widget.TextView;

import com.dawson.ces.feces.R;

import java.util.Date;

public class LoginSettingsActivity extends AppCompatActivity {

    private EditText firstName;
    private EditText lastName;
    private EditText email;
    private EditText password;
    private TextView lastUpdate;
    private Snackbar settingsChangedWarning;

    private int unsavedChanges = 0;

    /**
     * You can set some arguments via the intent
     * <p>
     * - boolean exitToDrawerOnSave - If once the user successfully saved this should start the
     * DrawerActivity
     *
     * @param savedInstanceState The saved instance state
     */
    @Override
    public void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.login_settings);

        Toolbar toolbar = findViewById(R.id.toolbar);
        setSupportActionBar(toolbar);

        this.firstName = findViewById(R.id.first_name_input);
        this.lastName = findViewById(R.id.last_name_input);
        this.email = findViewById(R.id.email_input);
        this.password = findViewById(R.id.password_input);
        this.lastUpdate = findViewById(R.id.last_update);

        updateSettingsFromPreferences();
        addSettingsChangedListeners();

        final FloatingActionButton fab = findViewById(R.id.fab);

        fab.setOnClickListener(view -> {
            if (this.validateSettings()) {
                this.saveSettings();
            }
        });
    }

    /**
     * Dismiss the settings changed warning, if shown
     */
    @Override
    public void onDestroy() {
        if (settingsChangedWarning != null && settingsChangedWarning.isShown()) {
            settingsChangedWarning.dismiss();
        }
        super.onDestroy();
    }

    /**
     * Handle the user pressing the back button to back out of the settings
     */
    @Override
    public void onBackPressed() {
        if (unsavedChanges > 0) {
            new AlertDialog.Builder(this)
                    .setTitle(R.string.unsaved_changes)
                    .setMessage(R.string.discard_changes)
                    .setPositiveButton(R.string.ok, (dialog, id) -> {
                        // It's time to finish this activity :(
                        finish();
                    })
                    .setNegativeButton(R.string.cancel, (dialog, id) -> {
                        // Don't do anything, just dismiss the dialog
                        // So the user can get back to editing
                        dialog.dismiss();
                    })
                    .create().show();
        } else {
            super.onBackPressed();
        }
    }

    /**
     * Validate the input of a few settings, showing a snackbar message if any are incorrect
     * <p>
     * - first name must not be empty - last name must not be empty - email must not be empty and
     * follow the basic form of "user@domain" - password must not be empty
     *
     * @return true If all settings are valid, false if any are incorrect
     */
    private boolean validateSettings() {

        if (this.firstName.getText().toString().isEmpty()) {
            this.showSnackbar(R.string.warning_name_empty);
            return false;
        }

        if (this.lastName.getText().toString().isEmpty()) {
            this.showSnackbar(R.string.warning_last_name_empty);
            return false;
        }

        if (this.email.getText().toString().isEmpty()
                || !Patterns.EMAIL_ADDRESS.matcher(email.getText().toString()).matches()) {
            this.showSnackbar(R.string.warning_email_invalid);
            return false;
        }

        if (this.password.getText().toString().isEmpty()) {
            this.showSnackbar(R.string.warning_password_empty);
            return false;
        }

        return true;
    }

    /**
     * Saves the settings to {@link SharedPreferences} (in private mode)
     */
    private void saveSettings() {
        SharedPreferences.Editor prefs =
                getSharedPreferences(getString(R.string.prefs_settings), MODE_PRIVATE).edit();

        prefs.putString(getString(R.string.prefs_first_name), firstName.getText().toString());
        prefs.putString(getString(R.string.prefs_last_name), lastName.getText().toString());
        prefs.putString(getString(R.string.prefs_email), email.getText().toString());
        prefs.putString(getString(R.string.prefs_password), password.getText().toString());

        // I would use java.time.Instant if we could use API 26+ :(
        // Instant.now().toString();
        Date d = new Date(System.currentTimeMillis());
        prefs.putLong(getString(R.string.prefs_last_modified), d.getTime());
        lastUpdate.setText(getString(R.string.last_update, d.toString()));

        // Handle it in the background
        prefs.apply();

        if (settingsChangedWarning != null && settingsChangedWarning.isShown()) {
            settingsChangedWarning.dismiss();
        }
        showSnackbar(R.string.settings_saved);
        unsavedChanges = 0;

        // Just finish the activity
        finish();
    }

    /**
     * Update the values displayed on the settings UI based on the current value of {@link
     * SharedPreferences}
     */
    private void updateSettingsFromPreferences() {
        SharedPreferences prefs =
                getSharedPreferences(getString(R.string.prefs_settings), MODE_PRIVATE);

        String firstNameIn = prefs.getString(getString(R.string.prefs_first_name), "");
        String lastNameIn = prefs.getString(getString(R.string.prefs_last_name), "");
        String emailIn = prefs.getString(getString(R.string.prefs_email), "");
        String passwordIn = prefs.getString(getString(R.string.prefs_password), "");

        this.firstName.setText(firstNameIn);
        this.lastName.setText(lastNameIn);
        this.email.setText(emailIn);
        this.password.setText(passwordIn);

        String lastUpdateTime;

        if (prefs.contains(getString(R.string.prefs_last_modified))) {
            long time =
                    prefs.getLong(getString(R.string.prefs_last_modified), System.currentTimeMillis());

            lastUpdateTime = new Date(time).toString();
        } else {
            lastUpdateTime = getString(R.string.never);
        }

        lastUpdate.setText(getString(R.string.last_update, lastUpdateTime));
    }

    /**
     * Attaches change listeners to the settings, which will listen for the user editing the settings
     * to new values and show a "Settings changed" notice
     */
    private void addSettingsChangedListeners() {
        this.firstName.addTextChangedListener(
                createTextChangeWatcher(this.firstName.getText().toString()));

        this.lastName.addTextChangedListener(
                createTextChangeWatcher(this.lastName.getText().toString()));

        this.email.addTextChangedListener(
                createTextChangeWatcher(this.email.getText().toString()));

        this.password.addTextChangedListener(
                createTextChangeWatcher(this.password.getText().toString()));
    }

    /**
     * Creates a new {@link TextWatcher} that watches for the value changing to something other than
     * the initial value provided, and then adjusts the unsaved changes counter accordingly
     *
     * @param initialValue The initial value the input had
     * @return A {@link TextWatcher} that can be attached
     */
    private TextWatcher createTextChangeWatcher(String initialValue) {
        return new TextWatcher() {
            private boolean unchanged = true;

            @Override
            public void beforeTextChanged(CharSequence s, int start, int count, int after) {
            }

            @Override
            public void onTextChanged(CharSequence s, int start, int before, int count) {
                if (s.toString().equals(initialValue)) {
                    adjustUnsavedChanges(unchanged ? 0 : -1);
                    unchanged = true;
                } else {
                    adjustUnsavedChanges(unchanged ? 1 : 0);
                    unchanged = false;
                }
            }

            @Override
            public void afterTextChanged(Editable s) {
            }
        };
    }

    /**
     * Adjust the count of unsaved changes by the specified amount, then displays/hides the snackbar
     * warning depending on the amount of unsavedChanges left
     *
     * @param adjustment The number of changes to adjust this by
     */
    private void adjustUnsavedChanges(int adjustment) {
        unsavedChanges += adjustment;

        if (unsavedChanges < 0) {
            unsavedChanges = 0;
        }

        if (unsavedChanges == 0) {
            if (settingsChangedWarning != null && settingsChangedWarning.isShown()) {
                settingsChangedWarning.dismiss();
            }
        } else {
            if (settingsChangedWarning == null || !settingsChangedWarning.isShown()) {
                settingsChangedWarning = showSnackbar(R.string.unsaved_changes, Snackbar.LENGTH_INDEFINITE);
            }
        }
    }

    /**
     * Show a snackbar message (with a default length of long)
     *
     * @param resId The resource id of the string you want to show
     */
    private Snackbar showSnackbar(int resId) {
        return showSnackbar(resId, Snackbar.LENGTH_LONG);
    }

    /**
     * Show a snackbar message
     *
     * @param resId  The resource id of the string you want to show
     * @param length The length you want to show the message for
     */
    private Snackbar showSnackbar(int resId, int length) {
        Snackbar sk = Snackbar.make(findViewById(R.id.container), resId, length);
        sk.show();
        return sk;
    }
}
