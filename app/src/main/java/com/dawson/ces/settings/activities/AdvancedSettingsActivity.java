package com.dawson.ces.settings.activities;

import android.content.Intent;
import android.content.SharedPreferences;
import android.os.Bundle;
import android.support.v7.app.AppCompatActivity;
import android.support.v7.widget.Toolbar;
import android.widget.CompoundButton;
import android.widget.CompoundButton.OnCheckedChangeListener;
import android.widget.Switch;

import com.dawson.ces.feces.R;

public class AdvancedSettingsActivity extends AppCompatActivity {

    private int unsavedChanges;
    private Switch backOpensDrawer;

    @Override
    public void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.advanced_settings);

        Toolbar toolbar = findViewById(R.id.toolbar);
        setSupportActionBar(toolbar);

        setTitle(R.string.advanced_settings);

        this.backOpensDrawer = findViewById(R.id.back_opens_drawer_input);

        SharedPreferences prefs =
                getSharedPreferences(getString(R.string.prefs_settings), MODE_PRIVATE);

        Intent i = getIntent();

        // Base the initial value off shared prefs rather then the bundle
        // Since the bundle can be different from the saved settings
        OnCheckedChangeListener listener = createRadioChangeListener(
                prefs.getBoolean(getString(R.string.prefs_back_opens_drawer), false)
        );

        this.backOpensDrawer.setOnCheckedChangeListener(listener);

        this.backOpensDrawer.setChecked(
                i.getBooleanExtra(getString(R.string.prefs_back_opens_drawer), false)
        );

        // Make sure the "unsaved changes" counter is updated correctly
        listener.onCheckedChanged(this.backOpensDrawer, this.backOpensDrawer.isChecked());
    }

    @Override
    public void onBackPressed() {
        Intent i = new Intent();

        i.putExtra(getString(R.string.prefs_unsaved_changes_count), this.unsavedChanges);
        i.putExtra(getString(R.string.prefs_back_opens_drawer), this.backOpensDrawer.isChecked());

        setResult(RESULT_OK, i);
        super.onBackPressed();
    }

    /**
     * Creates a new {@link android.widget.CompoundButton.OnCheckedChangeListener} that listens for the
     * value changing to something other than the initial id
     *
     * @param initial The initial value of the button
     * @return An {@link android.widget.CompoundButton.OnCheckedChangeListener} that can be attached
     */
    private CompoundButton.OnCheckedChangeListener createRadioChangeListener(boolean initial) {
        return new CompoundButton.OnCheckedChangeListener() {
            private boolean unchanged = true;

            @Override
            public void onCheckedChanged(CompoundButton button, boolean isChecked) {
                if (initial == isChecked) {
                    unsavedChanges += (unchanged ? 0 : -1);
                    unchanged = true;
                } else {
                    unsavedChanges += (unchanged ? 1 : 0);
                    unchanged = false;
                }
            }
        };
    }
}
