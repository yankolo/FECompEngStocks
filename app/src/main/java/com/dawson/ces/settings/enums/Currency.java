package com.dawson.ces.settings.enums;

import android.support.annotation.Nullable;

/**
 * Represents a currency type
 */
public enum Currency {
    CAD(0, "CAD"), USD(1, "USD"), BTC(2, "BTC");

    private int id;
    private String symbol;

    Currency(int id, String symbol) {
        this.id = id;
        this.symbol = symbol;
    }

    /**
     * @return id
     */
    public int getId() {
        return id;
    }

    /**
     * @return symbol
     */
    public String getSymbol() {
        return symbol;
    }

    /**
     * Get the matching currency for an id
     *
     * @param id The id of the currency you want to lookup
     * @return The currency, if found, else null
     */
    public static @Nullable
    Currency fromId(int id) {
        for (Currency currency : Currency.values()) {
            if (currency.id == id) {
                return currency;
            }
        }

        return null;
    }
}
